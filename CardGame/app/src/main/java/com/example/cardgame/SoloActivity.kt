package com.example.cardgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class SoloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.solo_layout)

        findViewById<Button>(R.id.play_solo_btn).setOnClickListener {
            val intent = Intent(this, SoloPlay2ply::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.rules_solo_btn).setOnClickListener {
            val intent = Intent(this, SoloRulesActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.back_solo_btn).setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}