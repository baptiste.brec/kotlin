package com.example.cardgame

import androidx.appcompat.app.AlertDialog
import android.annotation.SuppressLint
import android.content.Context

import android.content.Intent
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.cardgame.R
import com.example.cardgame.databinding.SoloRulesLayoutBinding

class SoloRulesActivity : AppCompatActivity() {

    private lateinit var binding:SoloRulesLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SoloRulesLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)



        binding.buttonBack.setOnClickListener{
            val intent = Intent(this, SoloActivity::class.java)
            startActivity(intent)
        }
    }
}
