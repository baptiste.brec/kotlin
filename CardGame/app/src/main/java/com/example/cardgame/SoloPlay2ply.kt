package com.example.cardgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.cardgame.databinding.SoloPlay2plyBinding
import kotlin.random.Random
import kotlin.collections.List

class SoloPlay2ply : AppCompatActivity() {
    private lateinit var binding: SoloPlay2plyBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = SoloPlay2plyBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.buttonFight.setVisibility(View.INVISIBLE)
        binding.buttonOk.setVisibility(View.INVISIBLE)
        binding.textFight.setVisibility(View.INVISIBLE)
        binding.buttonDiscard.setVisibility(View.INVISIBLE)
        HidePlayCards()
        HideChooseCards()


        var CardPlayed=0
        var PlayerPlayed=0
        var ActifPlayer=0
        var Cardshowned=0

        var CardChoosenP1=0
        var CardChoosenP2=0

        var Deck=arrayOf(arrayOf("As","Two","Three","Four"),arrayOf("1","2","3","4"),arrayOf("0","0","0","0"))
        var numbercardsP1=0
        var numbercardsP2=0

        var DeckP1=arrayOf(arrayOf("rien","rien","rien","rien"),arrayOf("0","0","0","0"),arrayOf("hand","hand","discard","discard"))
        var DeckP2=arrayOf(arrayOf("rien","rien","rien","rien"),arrayOf("0","0","0","0"),arrayOf("hand","hand","discard","discard"))

        var NewDeckP1: List<Int> = emptyList()
        var NewDeckP2: List<Int> = emptyList()
        //NewDeckP2=NewDeckP2+2
        //créer une liste et y ajouter des valeurs, puis quand la main est vide on se sert de cette liste pour créer la nouvelle main

        for (distribution in 0..3){
            var randomdistibution = Random.nextInt(1,3 )
            if (randomdistibution==1){
                if (numbercardsP1<2) {
                    DeckP1[0][numbercardsP1] = Deck[0][distribution]
                    DeckP1[1][numbercardsP1] = Deck[1][distribution]
                    numbercardsP1++
                }
                else{
                    DeckP2[0][numbercardsP2] = Deck[0][distribution]
                    DeckP2[1][numbercardsP2] = Deck[1][distribution]
                    numbercardsP2++
                }
            }
            else{
                if (numbercardsP2<2){
                    DeckP2[0][numbercardsP2] = Deck[0][distribution]
                    DeckP2[1][numbercardsP2] = Deck[1][distribution]
                    numbercardsP2++
                }
                else{
                    DeckP1[0][numbercardsP1] = Deck[0][distribution]
                    DeckP1[1][numbercardsP1] = Deck[1][distribution]
                    numbercardsP1++
                }
            }
        }

        binding.buttonHome.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        binding.buttonCardP1.setOnClickListener {
            ActifPlayer=1
            binding.buttonCardP1.setVisibility(View.INVISIBLE)
            binding.buttonCardP2.setVisibility(View.INVISIBLE)
            ShowChooseCards()
            Cardshowned= ShowCards(DeckP1, NewDeckP1)
            if (Cardshowned==-1){
                EndGame(ActifPlayer)
            }
            if (Cardshowned==-2){
                binding.buttonDiscard.setVisibility(View.VISIBLE)
            }
        }

        binding.buttonCardP2.setOnClickListener {
            ActifPlayer=2
            binding.buttonCardP1.setVisibility(View.INVISIBLE)
            binding.buttonCardP2.setVisibility(View.INVISIBLE)
            ShowChooseCards()
            Cardshowned= ShowCards(DeckP2, NewDeckP2)
            if (Cardshowned==-1){
                EndGame(ActifPlayer)
            }
            if (Cardshowned==-2){
                binding.buttonDiscard.setVisibility(View.VISIBLE)
            }
        }

        binding.buttonNext.setOnClickListener{
            if (Cardshowned<1){
                if (ActifPlayer==1)
                    Cardshowned= NextCard(DeckP1,Cardshowned)
                else
                    Cardshowned= NextCard(DeckP2,Cardshowned)
            }
        }

        binding.buttonPrevious.setOnClickListener {
            if (Cardshowned>0){
                if (ActifPlayer==1)
                    Cardshowned=PreviousCard(DeckP1,Cardshowned)
                else
                    Cardshowned=PreviousCard(DeckP2,Cardshowned)
            }
        }

        binding.buttonChoose.setOnClickListener {
            PlayerPlayed++
            CardPlayed++
            if (PlayerPlayed<2){
                if (ActifPlayer==1){
                    CardChoosenP1= ChooseCard(DeckP1,Cardshowned)
                    binding.buttonCardP2.setVisibility(View.VISIBLE)
                }
                else{
                    CardChoosenP2= ChooseCard(DeckP2,Cardshowned)
                    binding.buttonCardP1.setVisibility(View.VISIBLE)
                }
            }
            else{
                if (ActifPlayer==1){
                    CardChoosenP1= ChooseCard(DeckP1,Cardshowned)
                }
                else{
                    CardChoosenP2= ChooseCard(DeckP2,Cardshowned)
                }
                binding.buttonFight.setVisibility(View.VISIBLE)
            }
            HideChooseCards()
        }

        binding.buttonFight.setOnClickListener {
            binding.buttonFight.setVisibility(View.INVISIBLE)
            binding.textPlayCardP1.text=String.format("Player 1:\n %s",DeckP1[0][CardChoosenP1])
            binding.textPlayCardP2.text=String.format("Player 2:\n %s",DeckP2[0][CardChoosenP2])
            ShowPlayCards()
        }

        binding.buttonPlay.setOnClickListener {
            if (DeckP1[1][CardChoosenP1].toInt()<DeckP2[1][CardChoosenP2].toInt()){
                NewDeckP2=NewDeckP2+(DeckP1[1][CardChoosenP1]).toInt()
                NewDeckP2=NewDeckP2+(DeckP2[1][CardChoosenP2]).toInt()
                binding.textFight.text=String.format("Card Player 2 Higher")
            }
            else{
                NewDeckP1=NewDeckP1+(DeckP1[1][CardChoosenP1]).toInt()
                NewDeckP1=NewDeckP1+(DeckP2[1][CardChoosenP2]).toInt()
                    binding.textFight.text=String.format("Card Player 1 Higher")
            }
            HidePlayCards()
            binding.buttonOk.setVisibility(View.VISIBLE)
            binding.textFight.setVisibility(View.VISIBLE)

        }

        binding.buttonOk.setOnClickListener {
            PlayerPlayed=0
            binding.buttonOk.setVisibility(View.INVISIBLE)
            binding.textFight.setVisibility(View.INVISIBLE)
            binding.buttonCardP1.setVisibility(View.VISIBLE)
            binding.buttonCardP2.setVisibility(View.VISIBLE)
            if (CardPlayed%4==0){
                if (NewDeckP1.size==0){
                    EndGame(1)
                }
                if (NewDeckP2.size==0){
                    EndGame(2)
                }
            }
        }

        binding.buttonDiscard.setOnClickListener {
            if (ActifPlayer==1){
                Discard(DeckP1,NewDeckP1)
                ShowChooseCards()
                Cardshowned= ShowCards(DeckP1, NewDeckP1)
                NewDeckP1= emptyList()
            }
            else{
                Discard(DeckP2,NewDeckP2)
                ShowChooseCards()
                Cardshowned= ShowCards(DeckP2, NewDeckP2)
                NewDeckP2= emptyList()
            }
        }



    }

    private fun HideChooseCards(){
        binding.textChooseCard.setVisibility(View.INVISIBLE)
        binding.buttonChoose.setVisibility(View.INVISIBLE)
        binding.buttonPrevious.setVisibility(View.INVISIBLE)
        binding.buttonNext.setVisibility(View.INVISIBLE)
    }

    private fun ShowChooseCards(){
        binding.textChooseCard.setVisibility(View.VISIBLE)
        binding.buttonChoose.setVisibility(View.VISIBLE)
        binding.buttonPrevious.setVisibility(View.VISIBLE)
        binding.buttonNext.setVisibility(View.VISIBLE)
    }

    private fun HidePlayCards(){
        binding.buttonPlay.setVisibility(View.INVISIBLE)
        binding.textPlayCardP1.setVisibility(View.INVISIBLE)
        binding.textPlayCardP2.setVisibility(View.INVISIBLE)
    }

    private fun ShowPlayCards(){
        binding.buttonPlay.setVisibility(View.VISIBLE)
        binding.textPlayCardP1.setVisibility(View.VISIBLE)
        binding.textPlayCardP2.setVisibility(View.VISIBLE)
    }

    private fun ShowCards(Deck: Array<Array<String>>, NewDeck: List<Int>): Int{
        var okshow=0
        for ( i in 0..3){
            if (okshow==0){
                if (Deck[2][i]=="hand"){
                    okshow=1
                    binding.textChooseCard.text=String.format("%s",Deck[0][i])
                    return i
                }
            }
        }

        if (okshow==0){
            if (NewDeck.size==0){
                return -1
            }
            else{
                binding.buttonChoose.setVisibility(View.INVISIBLE)
                binding.buttonPrevious.setVisibility(View.INVISIBLE)
                binding.buttonNext.setVisibility(View.INVISIBLE)
                binding.buttonDiscard.setVisibility(View.VISIBLE)
                binding.textChooseCard.text=String.format("No more card in your hand")
                return -2
            }
        }
        return 0
    }

    private fun Discard(Deck: Array<Array<String>>, NewDeck: List<Int>){
        for (i in 0..NewDeck.size-1){
            if (NewDeck[i]==1){
                Deck[0][i]="As"
                Deck[1][i]="1"
                Deck[2][i]="hand"
            }
            else{
                if (NewDeck[i]==2){
                    Deck[0][i]="Two"
                    Deck[1][i]="2"
                    Deck[2][i]="hand"
                }
                else{
                    if (NewDeck[i]==3){
                        Deck[0][i]="Three"
                        Deck[1][i]="3"
                        Deck[2][i]="hand"
                    }
                    else{
                        Deck[0][i]="Four"
                        Deck[1][i]="4"
                        Deck[2][i]="hand"
                    }
                }
            }
        }
        binding.buttonDiscard.setVisibility(View.INVISIBLE)
    }


        private fun NextCard(Deck: Array<Array<String>>,Card: Int): Int{
        var oktry=0
        if (oktry==0){
            for (i in 1..3){
                if (i+Card<4){
                    if (Deck[2][Card+i]=="hand"){
                        binding.textChooseCard.text=String.format("%s",Deck[0][Card+i])
                        return (Card+i)
                    }
                }
            }
        }
        //binding.textChooseCard.text=String.format("coucou")
        return(Card)
    }


    private fun PreviousCard(Deck: Array<Array<String>>,Card: Int): Int{
        var oktry=0
        if (oktry==0){
            for (i in 1..Card){
                if (Deck[2][Card-i]=="hand"){
                    binding.textChooseCard.text=String.format("%s",Deck[0][Card-i])
                    return(Card-i)
                }
            }
        }
        return(Card)
    }

    private fun ChooseCard(Deck: Array<Array<String>>, Card: Int):Int{
        Deck[2][Card]="discard"
        return Card
    }

    private fun EndGame(ActifPlayer: Int){
        binding.textChooseCard.setVisibility(View.VISIBLE)
        binding.buttonChoose.setVisibility(View.INVISIBLE)
        binding.buttonPrevious.setVisibility(View.INVISIBLE)
        binding.buttonNext.setVisibility(View.INVISIBLE)
        binding.buttonCardP1.setVisibility(View.INVISIBLE)
        binding.buttonCardP2.setVisibility(View.INVISIBLE)
        if (ActifPlayer==1){
            binding.textChooseCard.text=String.format("No more card for Player 1.\nPlayer 2 won.")
        }
        else{
            binding.textChooseCard.text=String.format("No more card for Player 2.\nPlayer 1 won.")
        }
    }
}