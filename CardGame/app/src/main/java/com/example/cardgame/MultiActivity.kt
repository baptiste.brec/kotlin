package com.example.cardgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.view.View
import android.widget.Toast
import com.example.cardgame.databinding.MultiLayoutBinding
import com.example.cardgame.R
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class MultiActivity : AppCompatActivity() {
    private lateinit var binding: MultiLayoutBinding
    private lateinit var database: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = MultiLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)



        binding.backMultiBtn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        binding.buttonSend.setOnClickListener {
            val Name = binding.editTextTextPersonName.text.toString()
            val Report = binding.editTextText.text.toString()


            database = FirebaseDatabase.getInstance("https://cardgame-a42e4-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Users")
            val User = User(Name,Report)



            database.child(Name).setValue(User).addOnSuccessListener {
                Toast.makeText(baseContext, "Successfully Sended",
                    Toast.LENGTH_SHORT).show()
                binding.editTextTextPersonName.text.clear()
                binding.editTextText.text.clear()
            }.addOnFailureListener{
                Toast.makeText(baseContext, "Failed",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }
}