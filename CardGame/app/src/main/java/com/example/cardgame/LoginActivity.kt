package com.example.cardgame


import android.content.Intent
import android.net.MailTo
import android.os.Bundle
import android.text.style.UpdateLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.cardgame.databinding.LoginLayoutBinding

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

import android.util.Log
import android.widget.Toast

import com.google.firebase.auth.ktx.actionCodeSettings
import com.google.firebase.auth.ActionCodeSettings



import android.net.Uri
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.FirebaseException

import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.GithubAuthProvider
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PlayGamesAuthProvider
import com.google.firebase.auth.ktx.actionCodeSettings
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import java.util.concurrent.TimeUnit



class LoginActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private val TAG = "MainActivity"

    private lateinit var binding: LoginLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = LoginLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        auth= FirebaseAuth.getInstance()

        binding.buttonCreate.setOnClickListener {
            CreateUser()
        }

        binding.buttonLogin.setOnClickListener {
            doLogin()
        }
    }

    private fun doLogin() {
        auth.signInWithEmailAndPassword(binding.editTextMail.text.toString(), binding.editTextMDP.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(baseContext, "signInWithEmail succeed.",
                        Toast.LENGTH_SHORT).show()
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    Toast.makeText(baseContext, "signInWithEmail failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }
            }
    }


    public override fun onStart() {
        super.onStart()
        val currentUser = null
        }


    fun updateUI(currentUser:FirebaseUser?){
        if(currentUser!=null){
           val intent = Intent(this, SoloActivity::class.java)
            startActivity(intent)
        }else{
            Toast.makeText(baseContext, "signInWithEmail failed.",
                Toast.LENGTH_SHORT).show()
        }
    }

    fun CreateUser() {
        auth.createUserWithEmailAndPassword(binding.editTextMail.text.toString(), binding.editTextMDP.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")
                    Toast.makeText(baseContext, "Authentication succed.",
                        Toast.LENGTH_SHORT).show()
                    binding.editTextMail.getText().clear()
                    binding.editTextMDP.getText().clear()
                    val user = auth.currentUser
                    updateUI(null)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }
            }
    }
}

