package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.VISIBLE
import android.widget.Toast

import com.example.apero.databinding.RouletteLayoutBinding
import kotlin.random.Random


class RouletteActivity : AppCompatActivity() {

    private lateinit var binding: RouletteLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = RouletteLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.rouletteretourbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        binding.rouletteresetbutton.setOnClickListener {
            val intent = Intent(this, RouletteActivity::class.java)
            startActivity(intent)
        }



        cacher()
        cachertirage()


        var nbJ=2
        var nbJactif=1

        val roulette = listOf(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36)
        var previoustabl= mutableListOf<Int>()

        var pointsJ= mutableListOf<Int>()
        var miserJ:MutableList<Array<Int>> = mutableListOf()


        var jouees= mutableListOf(false,false,false,false,false,false,false,false,false,false,false,false)



        binding.roulettemoinsbutton.setOnClickListener {
            if (nbJ>2){
                nbJ--
                binding.roulettenbjtextView.text=String.format("Nombre de joueurs:\n%d",nbJ)
            }
        }

        binding.rouletteplusbutton.setOnClickListener {
            nbJ++
            binding.roulettenbjtextView.text=String.format("Nombre de joueurs:\n%d",nbJ)
        }

        binding.roulettevalidernbjbutton.setOnClickListener {
            binding.roulettevalidernbjbutton.visibility= View.INVISIBLE
            binding.roulettemoinsbutton.visibility= View.INVISIBLE
            binding.rouletteplusbutton.visibility= View.INVISIBLE
            binding.roulettenbjtextView.visibility= View.INVISIBLE

            pointsJ=creerpoints(nbJ,pointsJ)
            afficherpts(pointsJ[nbJactif-1])


            miserJ=creertablejoueur(nbJ)

            afficher()
        }

        binding.roulettenextbutton.setOnClickListener {
            nbJactif++
            if(nbJactif>nbJ)
                nbJactif=1
            binding.rouletteJoueurtextView.text=String.format("Joueur %d",nbJactif)

            afficherpts(pointsJ[nbJactif-1])
        }

        binding.roulettepreviousbutton.setOnClickListener {
            nbJactif--
            if(nbJactif==0)
                nbJactif=nbJ
            binding.rouletteJoueurtextView.text=String.format("Joueur %d",nbJactif)

            afficherpts(pointsJ[nbJactif-1])
        }

        binding.roulettetiragebutton.setOnClickListener {
            val nb = roulette[Random.nextInt(0,roulette.size)]
            binding.rouletteTiragetextView.text=String.format("%d",nb)
            binding.rouletteTiragetextView.setTextColor(couleur(nb))
            cacher()
            affichertirage()

            previoustabl=previous(nb,previoustabl)
            pointsJ=resultats(nb,miserJ,pointsJ,jouees)
        }

        binding.rouletteMiserbutton.setOnClickListener {
            cachertirage()
            afficher()
            afficherprevious(previoustabl)
            nbJactif=1
            afficherpts(pointsJ[nbJactif-1])
            binding.rouletteJoueurtextView.text=String.format("Joueur %d",nbJactif)

            miserJ=creertablejoueur(nbJ)
            jouees= mutableListOf(false,false,false,false,false,false,false,false,false,false,false,false)
        }

        binding.rouletteOkbutton.setOnClickListener {
            cacher()
            afficher()
            afficherprevious(previoustabl)
        }

        binding.rouletteRNbutton.setOnClickListener {
            cacher()
            afficherprevious(previoustabl)
            afficherRN()
            binding.roulettepointsJtextView.visibility=VISIBLE

            binding.rouletteNoirNbeditTextNumber.text.clear()
            binding.rouletteRougeMisereditTextNumber.text.clear()
            binding.rouletteNoirNbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][0])
            binding.rouletteRougeMisereditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][1])
        }
        binding.rouletteNoirMiserbutton.setOnClickListener {
            if (binding.rouletteNoirNbeditTextNumber.text.toString()!="" && binding.rouletteNoirNbeditTextNumber.text.toString().toInt()>=0){
                val miser=binding.rouletteNoirNbeditTextNumber.text.toString().toInt()

                if (miser<=pointsJ[nbJactif-1]+binding.rouletteNoirNbeditTextNumber.hint.toString().toInt()){
                    miserJ[nbJactif-1][0]=miser

                    if (miser>binding.rouletteNoirNbeditTextNumber.hint.toString().toInt())
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.rouletteNoirNbeditTextNumber.hint.toString().toInt()-miser
                    else
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.rouletteNoirNbeditTextNumber.hint.toString().toInt()-miser
                }
                else{
                    Toast.makeText(baseContext, "Pas assez de points...",
                        Toast.LENGTH_SHORT).show()
                }
                binding.rouletteNoirNbeditTextNumber.text.clear()
                binding.rouletteNoirNbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][0])
                afficherpts(pointsJ[nbJactif-1])

                jouees[0]=true
            }
        }
        binding.rouletteRougeMiserbutton.setOnClickListener {
            if (binding.rouletteRougeMisereditTextNumber.text.toString()!="" && binding.rouletteRougeMisereditTextNumber.text.toString().toInt()>=0){
                val miser=binding.rouletteRougeMisereditTextNumber.text.toString().toInt()

                if (miser<=pointsJ[nbJactif-1]+binding.rouletteRougeMisereditTextNumber.hint.toString().toInt()){
                    miserJ[nbJactif-1][1]=miser

                    if (miser>binding.rouletteRougeMisereditTextNumber.hint.toString().toInt())
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.rouletteRougeMisereditTextNumber.hint.toString().toInt()-miser
                    else
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.rouletteRougeMisereditTextNumber.hint.toString().toInt()-miser
                }
                else{
                    Toast.makeText(baseContext, "Pas assez de points...",
                        Toast.LENGTH_SHORT).show()
                }
                binding.rouletteRougeMisereditTextNumber.text.clear()
                binding.rouletteRougeMisereditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][1])
                afficherpts(pointsJ[nbJactif-1])

                jouees[0]=true
            }
        }


        binding.roulettePIbutton.setOnClickListener {
            cacher()
            afficherprevious(previoustabl)
            afficherPI()
            binding.roulettepointsJtextView.visibility=VISIBLE

            binding.roulettePairNbeditTextNumber.text.clear()
            binding.rouletteImpairMisereditTextNumber.text.clear()
            binding.roulettePairNbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][2])
            binding.rouletteImpairMisereditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][3])
        }
        binding.roulettePairMiserbutton.setOnClickListener {
            if (binding.roulettePairNbeditTextNumber.text.toString()!="" && binding.roulettePairNbeditTextNumber.text.toString().toInt()>=0){
                val miser=binding.roulettePairNbeditTextNumber.text.toString().toInt()

                if (miser<=pointsJ[nbJactif-1]+binding.roulettePairNbeditTextNumber.hint.toString().toInt()){
                    miserJ[nbJactif-1][2]=miser

                    if (miser>binding.roulettePairNbeditTextNumber.hint.toString().toInt())
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.roulettePairNbeditTextNumber.hint.toString().toInt()-miser
                    else
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.roulettePairNbeditTextNumber.hint.toString().toInt()-miser
                }
                else{
                    Toast.makeText(baseContext, "Pas assez de points...",
                        Toast.LENGTH_SHORT).show()
                }
                binding.roulettePairNbeditTextNumber.text.clear()
                binding.roulettePairNbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][2])
                afficherpts(pointsJ[nbJactif-1])

                jouees[1]=true
            }
        }
        binding.rouletteImpairMiserbutton.setOnClickListener {
            if (binding.rouletteImpairMisereditTextNumber.text.toString()!="" && binding.rouletteImpairMisereditTextNumber.text.toString().toInt()>=0){
                val miser=binding.rouletteImpairMisereditTextNumber.text.toString().toInt()

                if (miser<=pointsJ[nbJactif-1]+binding.rouletteImpairMisereditTextNumber.hint.toString().toInt()){
                    miserJ[nbJactif-1][3]=miser

                    if (miser>binding.rouletteImpairMisereditTextNumber.hint.toString().toInt())
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.rouletteImpairMisereditTextNumber.hint.toString().toInt()-miser
                    else
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.rouletteImpairMisereditTextNumber.hint.toString().toInt()-miser
                }
                else{
                    Toast.makeText(baseContext, "Pas assez de points...",
                        Toast.LENGTH_SHORT).show()
                }
                binding.rouletteImpairMisereditTextNumber.text.clear()
                binding.rouletteImpairMisereditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][3])
                afficherpts(pointsJ[nbJactif-1])

                jouees[1]=true
            }
        }

        binding.rouletteMoitiebutton.setOnClickListener {
            cacher()
            afficherprevious(previoustabl)
            affichermoitie()
            binding.roulettepointsJtextView.visibility=VISIBLE

            binding.roulette118NbeditTextNumber.text.clear()
            binding.roulette1936NbeditTextNumber.text.clear()
            binding.roulette118NbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][4])
            binding.roulette1936NbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][5])
        }
        binding.roulette118Miserbutton.setOnClickListener {
            if (binding.roulette118NbeditTextNumber.text.toString()!="" && binding.roulette118NbeditTextNumber.text.toString().toInt()>=0){
                val miser=binding.roulette118NbeditTextNumber.text.toString().toInt()

                if (miser<=pointsJ[nbJactif-1]+binding.roulette118NbeditTextNumber.hint.toString().toInt()){
                    miserJ[nbJactif-1][4]=miser

                    if (miser>binding.roulette118NbeditTextNumber.hint.toString().toInt())
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.roulette118NbeditTextNumber.hint.toString().toInt()-miser
                    else
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.roulette118NbeditTextNumber.hint.toString().toInt()-miser
                }
                else{
                    Toast.makeText(baseContext, "Pas assez de points...",
                        Toast.LENGTH_SHORT).show()
                }
                binding.roulette118NbeditTextNumber.text.clear()
                binding.roulette118NbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][4])
                afficherpts(pointsJ[nbJactif-1])

                jouees[2]=true
            }
        }
        binding.roulette1936Miserbutton.setOnClickListener {
            if (binding.roulette1936NbeditTextNumber.text.toString()!="" && binding.roulette1936NbeditTextNumber.text.toString().toInt()>=0){
                val miser=binding.roulette1936NbeditTextNumber.text.toString().toInt()

                if (miser<=pointsJ[nbJactif-1]+binding.roulette1936NbeditTextNumber.hint.toString().toInt()){
                    miserJ[nbJactif-1][5]=miser

                    if (miser>binding.roulette1936NbeditTextNumber.hint.toString().toInt())
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.roulette1936NbeditTextNumber.hint.toString().toInt()-miser
                    else
                        pointsJ[nbJactif-1]=pointsJ[nbJactif-1]+binding.roulette1936NbeditTextNumber.hint.toString().toInt()-miser
                }
                else{
                    Toast.makeText(baseContext, "Pas assez de points...",
                        Toast.LENGTH_SHORT).show()
                }
                binding.roulette1936NbeditTextNumber.text.clear()
                binding.roulette1936NbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][5])
                afficherpts(pointsJ[nbJactif-1])

                jouees[2]=true
            }
        }

        binding.roulettelignebutton.setOnClickListener {
            cacher()
            afficherprevious(previoustabl)
            afficherligne()
            binding.roulettepointsJtextView.visibility=VISIBLE

            binding.rouletteLigne1NbeditTextNumber.text.clear()
            binding.rouletteLigne2NbeditTextNumber.text.clear()
            binding.rouletteLigne3NbeditTextNumber.text.clear()
            binding.rouletteLigne1NbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][6])
            binding.rouletteLigne2NbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][7])
            binding.rouletteLigne3NbeditTextNumber.hint=String.format("%d",miserJ[nbJactif-1][8])
        }


    }

    private fun resultats(numero:Int,miserJtabl:MutableList<Array<Int>>,ptsJtable:MutableList<Int>,jouees:MutableList<Boolean>):MutableList<Int>{
        var tablgains=IntArray(miserJtabl.size)
        if (jouees[0]){
            for (i in 0..miserJtabl.size-1){
                if (numero==32 || numero==19 || numero==21 || numero==25 || numero==34 || numero==27 || numero==36 || numero==30 || numero==23 || numero==5 || numero==16 || numero==1 || numero==14 || numero==9 || numero==18 || numero==7 || numero==12 || numero==3){
                    tablgains[i]+=miserJtabl[i][1]*2
                }
                else{
                    if (numero==0){
                        tablgains[i]+=miserJtabl[i][1]+miserJtabl[i][0]
                    }
                    else{
                        tablgains[i]+=miserJtabl[i][0]*2
                    }
                }
            }
        }

        if (jouees[1]){
            for (i in 0..miserJtabl.size-1){
                if (numero%2==0){
                    tablgains[i]+=miserJtabl[i][2]*2
                }
                else{
                    if (numero==0){
                        tablgains[i]+=miserJtabl[i][2]+miserJtabl[i][3]
                    }
                    else{
                        tablgains[i]+=miserJtabl[i][3]*2
                    }
                }
            }
        }

        if (jouees[2]){
            for (i in 0..miserJtabl.size-1){
                if (numero==0){
                    tablgains[i]+=miserJtabl[i][4]+miserJtabl[i][5]
                }
                else{
                    if (numero<19){
                        tablgains[i]+=miserJtabl[i][4]*2
                    }
                    else{
                        tablgains[i]+=miserJtabl[i][5]*2
                    }
                }
            }
        }

        binding.rouletteNoirNbeditTextNumber.text.clear()
        binding.rouletteResultattextView.text=String.format("")

        var textresults=""
        for (i in 0..tablgains.size-1){
            textresults+=String.format("Joueur %d gagne %d\n",i+1,tablgains[i])
            ptsJtable[i]+=tablgains[i]
        }



        binding.rouletteResultattextView.text=String.format("%s",textresults)
        return(ptsJtable)
    }

    private fun afficherpts(nbpts:Int){
        binding.roulettepointsJtextView.text=String.format("Nombre de points: %d",nbpts)
    }

    private fun creerpoints(nbJ:Int,table: MutableList<Int>):MutableList<Int>{
        for (i in 1..nbJ) {
            table.add(10)
        }
        return(table)
    }

    private fun previous(numero:Int,table:MutableList<Int>):MutableList<Int>{
        if (table.size!=10){
            table.add(numero)
            return(table)
        }
        for (elt in 0..8){
            table[elt]=table[elt+1]
        }
        table[9]=numero
        return(table)
    }

    private fun creertablejoueur(nbJ:Int):MutableList<Array<Int>>{
        var miserJ:MutableList<Array<Int>> = mutableListOf()

        for (i in 1..nbJ)
            miserJ.add(arrayOf(0,0,0,0,0,0,0,0,0))


        return(miserJ)

    }

    private fun cachertirage(){
        binding.rouletteTiragetextView.visibility=View.INVISIBLE
        binding.rouletteResultattextView.visibility=View.INVISIBLE
        binding.rouletteMiserbutton.visibility=View.INVISIBLE
    }

    private fun affichertirage(){
        binding.rouletteTiragetextView.visibility=View.VISIBLE
        binding.rouletteResultattextView.visibility=View.VISIBLE
        binding.rouletteMiserbutton.visibility=View.VISIBLE
    }

    private fun couleur(numero:Int):Int{
        if (numero==0)
            return((4278255385/2)*2)
        else{
            if (numero==32 || numero==19 || numero==21 || numero==25 || numero==34 || numero==27 || numero==36 || numero==30 || numero==23 || numero==5 || numero==16 || numero==1 || numero==14 || numero==9 || numero==18 || numero==7 || numero==12 || numero==3)
                return((4294901760/2)*2)
            else
                return ((4278190080/2)*2)
        }
    }

    private fun cacher(){
        binding.roulettepreviousbutton.visibility=View.INVISIBLE
        binding.roulettenextbutton.visibility=View.INVISIBLE
        binding.rouletteJoueurtextView.visibility=View.INVISIBLE
        binding.rouletteprevioustiragestextView.visibility=View.INVISIBLE

        binding.roulettepointsJtextView.visibility=View.INVISIBLE
        binding.roulettedistribuerbutton.visibility=View.INVISIBLE
        binding.roulettetiragebutton.visibility=View.INVISIBLE
        binding.rouletterecapbutton.visibility=View.INVISIBLE
        binding.roulettepilebutton.visibility=View.INVISIBLE
        binding.roulettechevalbutton.visibility=View.INVISIBLE
        binding.roulettetransversalebutton.visibility=View.INVISIBLE
        binding.roulettecarrebutton.visibility=View.INVISIBLE
        binding.roulettesixainbutton.visibility=View.INVISIBLE
        binding.roulette12button.visibility=View.INVISIBLE
        binding.roulettelignebutton.visibility=View.INVISIBLE
        binding.rouletteMoitiebutton.visibility=View.INVISIBLE
        binding.roulettePIbutton.visibility=View.INVISIBLE
        binding.rouletteRNbutton.visibility=View.INVISIBLE

        binding.rouletteprevioustirage1textView.visibility=View.INVISIBLE
        binding.rouletteprevioustirage2textView.visibility=View.INVISIBLE
        binding.rouletteprevioustirage3textView.visibility=View.INVISIBLE
        binding.rouletteprevioustirage4textView.visibility=View.INVISIBLE
        binding.rouletteprevioustirage5textView.visibility=View.INVISIBLE
        binding.rouletteprevioustirage6textView.visibility=View.INVISIBLE
        binding.rouletteprevioustirage7textView.visibility=View.INVISIBLE
        binding.rouletteprevioustirage8textView.visibility=View.INVISIBLE
        binding.rouletteprevioustirage9textView.visibility=View.INVISIBLE
        binding.rouletteprevioustirage10textView.visibility=View.INVISIBLE

        binding.rouletteNoirtextView.visibility=View.INVISIBLE
        binding.rouletteNoirNbeditTextNumber.visibility=View.INVISIBLE
        binding.rouletteNoirMiserbutton.visibility=View.INVISIBLE
        binding.rouletteRougetextView.visibility=View.INVISIBLE
        binding.rouletteRougeMisereditTextNumber.visibility=View.INVISIBLE
        binding.rouletteRougeMiserbutton.visibility=View.INVISIBLE
        binding.rouletteOkbutton.visibility=View.INVISIBLE

        binding.roulettePairtextView.visibility=View.INVISIBLE
        binding.roulettePairNbeditTextNumber.visibility=View.INVISIBLE
        binding.roulettePairMiserbutton.visibility=View.INVISIBLE
        binding.rouletteImpairtextView.visibility=View.INVISIBLE
        binding.rouletteImpairMisereditTextNumber.visibility=View.INVISIBLE
        binding.rouletteImpairMiserbutton.visibility=View.INVISIBLE

        binding.roulette118textView.visibility=View.INVISIBLE
        binding.roulette118NbeditTextNumber.visibility=View.INVISIBLE
        binding.roulette118Miserbutton.visibility=View.INVISIBLE
        binding.roulette1936textView.visibility=View.INVISIBLE
        binding.roulette1936NbeditTextNumber.visibility=View.INVISIBLE
        binding.roulette1936Miserbutton.visibility=View.INVISIBLE

        binding.rouletteLigne1textView.visibility=View.INVISIBLE
        binding.rouletteLigne1NbeditTextNumber.visibility=View.INVISIBLE
        binding.rouletteLigne1Miserbutton.visibility=View.INVISIBLE
        binding.rouletteLigne2textView.visibility=View.INVISIBLE
        binding.rouletteLigne2NbeditTextNumber.visibility=View.INVISIBLE
        binding.rouletteLigne2Miserbutton.visibility=View.INVISIBLE
        binding.rouletteLigne3textView.visibility=View.INVISIBLE
        binding.rouletteLigne3NbeditTextNumber.visibility=View.INVISIBLE
        binding.rouletteLigne3Miserbutton.visibility=View.INVISIBLE
    }

    private fun afficher(){
        binding.roulettepreviousbutton.visibility= VISIBLE
        binding.roulettenextbutton.visibility= VISIBLE
        binding.rouletteJoueurtextView.visibility= VISIBLE
        binding.rouletteprevioustiragestextView.visibility= VISIBLE

        binding.roulettepointsJtextView.visibility= VISIBLE
        binding.roulettedistribuerbutton.visibility= VISIBLE
        binding.roulettetiragebutton.visibility= VISIBLE
        binding.rouletterecapbutton.visibility= VISIBLE
        binding.roulettepilebutton.visibility= VISIBLE
        binding.roulettechevalbutton.visibility= VISIBLE
        binding.roulettetransversalebutton.visibility= VISIBLE
        binding.roulettecarrebutton.visibility= VISIBLE
        binding.roulettesixainbutton.visibility= VISIBLE
        binding.roulette12button.visibility= VISIBLE
        binding.roulettelignebutton.visibility= VISIBLE
        binding.rouletteMoitiebutton.visibility= VISIBLE
        binding.roulettePIbutton.visibility= VISIBLE
        binding.rouletteRNbutton.visibility= VISIBLE
    }

    private fun afficherprevious(table:MutableList<Int>){
        binding.rouletteprevioustiragestextView.visibility= VISIBLE

        binding.rouletteprevioustirage1textView.visibility=View.GONE
        binding.rouletteprevioustirage2textView.visibility=View.GONE
        binding.rouletteprevioustirage3textView.visibility=View.GONE
        binding.rouletteprevioustirage4textView.visibility=View.GONE
        binding.rouletteprevioustirage5textView.visibility=View.GONE
        binding.rouletteprevioustirage6textView.visibility=View.GONE
        binding.rouletteprevioustirage7textView.visibility=View.GONE
        binding.rouletteprevioustirage8textView.visibility=View.GONE
        binding.rouletteprevioustirage9textView.visibility=View.GONE
        binding.rouletteprevioustirage10textView.visibility=View.GONE

        if(table.size==10){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))

            binding.rouletteprevioustirage2textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage2textView.text=String.format("%d",table[1])
            binding.rouletteprevioustirage2textView.setTextColor(couleur(table[1]))

            binding.rouletteprevioustirage3textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage3textView.text=String.format("%d",table[2])
            binding.rouletteprevioustirage3textView.setTextColor(couleur(table[2]))

            binding.rouletteprevioustirage4textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage4textView.text=String.format("%d",table[3])
            binding.rouletteprevioustirage4textView.setTextColor(couleur(table[3]))

            binding.rouletteprevioustirage5textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage5textView.text=String.format("%d",table[4])
            binding.rouletteprevioustirage5textView.setTextColor(couleur(table[4]))

            binding.rouletteprevioustirage6textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage6textView.text=String.format("%d",table[5])
            binding.rouletteprevioustirage6textView.setTextColor(couleur(table[5]))

            binding.rouletteprevioustirage7textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage7textView.text=String.format("%d",table[6])
            binding.rouletteprevioustirage7textView.setTextColor(couleur(table[6]))

            binding.rouletteprevioustirage8textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage8textView.text=String.format("%d",table[7])
            binding.rouletteprevioustirage8textView.setTextColor(couleur(table[7]))

            binding.rouletteprevioustirage9textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage9textView.text=String.format("%d",table[8])
            binding.rouletteprevioustirage9textView.setTextColor(couleur(table[8]))
        }

        if(table.size==1){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))
        }

        if(table.size==2){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))

            binding.rouletteprevioustirage2textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage2textView.text=String.format("%d",table[1])
            binding.rouletteprevioustirage2textView.setTextColor(couleur(table[1]))
        }

        if(table.size==3){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))

            binding.rouletteprevioustirage2textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage2textView.text=String.format("%d",table[1])
            binding.rouletteprevioustirage2textView.setTextColor(couleur(table[1]))

            binding.rouletteprevioustirage3textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage3textView.text=String.format("%d",table[2])
            binding.rouletteprevioustirage3textView.setTextColor(couleur(table[2]))
        }

        if(table.size==4){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))

            binding.rouletteprevioustirage2textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage2textView.text=String.format("%d",table[1])
            binding.rouletteprevioustirage2textView.setTextColor(couleur(table[1]))

            binding.rouletteprevioustirage3textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage3textView.text=String.format("%d",table[2])
            binding.rouletteprevioustirage3textView.setTextColor(couleur(table[2]))

            binding.rouletteprevioustirage4textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage4textView.text=String.format("%d",table[3])
            binding.rouletteprevioustirage4textView.setTextColor(couleur(table[3]))
        }

        if(table.size==5){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))

            binding.rouletteprevioustirage2textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage2textView.text=String.format("%d",table[1])
            binding.rouletteprevioustirage2textView.setTextColor(couleur(table[1]))

            binding.rouletteprevioustirage3textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage3textView.text=String.format("%d",table[2])
            binding.rouletteprevioustirage3textView.setTextColor(couleur(table[2]))

            binding.rouletteprevioustirage4textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage4textView.text=String.format("%d",table[3])
            binding.rouletteprevioustirage4textView.setTextColor(couleur(table[3]))

            binding.rouletteprevioustirage5textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage5textView.text=String.format("%d",table[4])
            binding.rouletteprevioustirage5textView.setTextColor(couleur(table[4]))
        }

        if(table.size==6){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))

            binding.rouletteprevioustirage2textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage2textView.text=String.format("%d",table[1])
            binding.rouletteprevioustirage2textView.setTextColor(couleur(table[1]))

            binding.rouletteprevioustirage3textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage3textView.text=String.format("%d",table[2])
            binding.rouletteprevioustirage3textView.setTextColor(couleur(table[2]))

            binding.rouletteprevioustirage4textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage4textView.text=String.format("%d",table[3])
            binding.rouletteprevioustirage4textView.setTextColor(couleur(table[3]))

            binding.rouletteprevioustirage5textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage5textView.text=String.format("%d",table[4])
            binding.rouletteprevioustirage5textView.setTextColor(couleur(table[4]))

            binding.rouletteprevioustirage6textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage6textView.text=String.format("%d",table[5])
            binding.rouletteprevioustirage6textView.setTextColor(couleur(table[5]))
        }

        if(table.size==7){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))

            binding.rouletteprevioustirage2textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage2textView.text=String.format("%d",table[1])
            binding.rouletteprevioustirage2textView.setTextColor(couleur(table[1]))

            binding.rouletteprevioustirage3textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage3textView.text=String.format("%d",table[2])
            binding.rouletteprevioustirage3textView.setTextColor(couleur(table[2]))

            binding.rouletteprevioustirage4textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage4textView.text=String.format("%d",table[3])
            binding.rouletteprevioustirage4textView.setTextColor(couleur(table[3]))

            binding.rouletteprevioustirage5textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage5textView.text=String.format("%d",table[4])
            binding.rouletteprevioustirage5textView.setTextColor(couleur(table[4]))

            binding.rouletteprevioustirage6textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage6textView.text=String.format("%d",table[5])
            binding.rouletteprevioustirage6textView.setTextColor(couleur(table[5]))

            binding.rouletteprevioustirage7textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage7textView.text=String.format("%d",table[6])
            binding.rouletteprevioustirage7textView.setTextColor(couleur(table[6]))
        }

        if(table.size==8){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))

            binding.rouletteprevioustirage2textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage2textView.text=String.format("%d",table[1])
            binding.rouletteprevioustirage2textView.setTextColor(couleur(table[1]))

            binding.rouletteprevioustirage3textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage3textView.text=String.format("%d",table[2])
            binding.rouletteprevioustirage3textView.setTextColor(couleur(table[2]))

            binding.rouletteprevioustirage4textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage4textView.text=String.format("%d",table[3])
            binding.rouletteprevioustirage4textView.setTextColor(couleur(table[3]))

            binding.rouletteprevioustirage5textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage5textView.text=String.format("%d",table[4])
            binding.rouletteprevioustirage5textView.setTextColor(couleur(table[4]))

            binding.rouletteprevioustirage6textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage6textView.text=String.format("%d",table[5])
            binding.rouletteprevioustirage6textView.setTextColor(couleur(table[5]))

            binding.rouletteprevioustirage7textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage7textView.text=String.format("%d",table[6])
            binding.rouletteprevioustirage7textView.setTextColor(couleur(table[6]))

            binding.rouletteprevioustirage8textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage8textView.text=String.format("%d",table[7])
            binding.rouletteprevioustirage8textView.setTextColor(couleur(table[7]))
        }

        if(table.size==9){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))

            binding.rouletteprevioustirage2textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage2textView.text=String.format("%d",table[1])
            binding.rouletteprevioustirage2textView.setTextColor(couleur(table[1]))

            binding.rouletteprevioustirage3textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage3textView.text=String.format("%d",table[2])
            binding.rouletteprevioustirage3textView.setTextColor(couleur(table[2]))

            binding.rouletteprevioustirage4textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage4textView.text=String.format("%d",table[3])
            binding.rouletteprevioustirage4textView.setTextColor(couleur(table[3]))

            binding.rouletteprevioustirage5textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage5textView.text=String.format("%d",table[4])
            binding.rouletteprevioustirage5textView.setTextColor(couleur(table[4]))

            binding.rouletteprevioustirage6textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage6textView.text=String.format("%d",table[5])
            binding.rouletteprevioustirage6textView.setTextColor(couleur(table[5]))

            binding.rouletteprevioustirage7textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage7textView.text=String.format("%d",table[6])
            binding.rouletteprevioustirage7textView.setTextColor(couleur(table[6]))

            binding.rouletteprevioustirage8textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage8textView.text=String.format("%d",table[7])
            binding.rouletteprevioustirage8textView.setTextColor(couleur(table[7]))

            binding.rouletteprevioustirage9textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage9textView.text=String.format("%d",table[8])
            binding.rouletteprevioustirage9textView.setTextColor(couleur(table[8]))
        }

        if(table.size==10){
            binding.rouletteprevioustirage1textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage1textView.text=String.format("%d",table[0])
            binding.rouletteprevioustirage1textView.setTextColor(couleur(table[0]))

            binding.rouletteprevioustirage2textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage2textView.text=String.format("%d",table[1])
            binding.rouletteprevioustirage2textView.setTextColor(couleur(table[1]))

            binding.rouletteprevioustirage3textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage3textView.text=String.format("%d",table[2])
            binding.rouletteprevioustirage3textView.setTextColor(couleur(table[2]))

            binding.rouletteprevioustirage4textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage4textView.text=String.format("%d",table[3])
            binding.rouletteprevioustirage4textView.setTextColor(couleur(table[3]))

            binding.rouletteprevioustirage5textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage5textView.text=String.format("%d",table[4])
            binding.rouletteprevioustirage5textView.setTextColor(couleur(table[4]))

            binding.rouletteprevioustirage6textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage6textView.text=String.format("%d",table[5])
            binding.rouletteprevioustirage6textView.setTextColor(couleur(table[5]))

            binding.rouletteprevioustirage7textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage7textView.text=String.format("%d",table[6])
            binding.rouletteprevioustirage7textView.setTextColor(couleur(table[6]))

            binding.rouletteprevioustirage8textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage8textView.text=String.format("%d",table[7])
            binding.rouletteprevioustirage8textView.setTextColor(couleur(table[7]))

            binding.rouletteprevioustirage9textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage9textView.text=String.format("%d",table[8])
            binding.rouletteprevioustirage9textView.setTextColor(couleur(table[8]))

            binding.rouletteprevioustirage10textView.visibility=View.VISIBLE
            binding.rouletteprevioustirage10textView.text=String.format("%d",table[9])
            binding.rouletteprevioustirage10textView.setTextColor(couleur(table[9]))
        }
    }

    private fun afficherRN(){
        binding.rouletteNoirtextView.visibility=VISIBLE
        binding.rouletteNoirNbeditTextNumber.visibility=VISIBLE
        binding.rouletteNoirMiserbutton.visibility=VISIBLE
        binding.rouletteRougetextView.visibility=VISIBLE
        binding.rouletteRougeMisereditTextNumber.visibility=VISIBLE
        binding.rouletteRougeMiserbutton.visibility=VISIBLE
        binding.rouletteOkbutton.visibility=VISIBLE
    }

    private fun afficherPI(){
        binding.roulettePairtextView.visibility=VISIBLE
        binding.roulettePairNbeditTextNumber.visibility=VISIBLE
        binding.roulettePairMiserbutton.visibility=VISIBLE
        binding.rouletteImpairtextView.visibility=VISIBLE
        binding.rouletteImpairMisereditTextNumber.visibility=VISIBLE
        binding.rouletteImpairMiserbutton.visibility=VISIBLE
        binding.rouletteOkbutton.visibility=VISIBLE
    }

    private fun affichermoitie(){
        binding.roulette118textView.visibility=VISIBLE
        binding.roulette118NbeditTextNumber.visibility=VISIBLE
        binding.roulette118Miserbutton.visibility=VISIBLE
        binding.roulette1936textView.visibility=VISIBLE
        binding.roulette1936NbeditTextNumber.visibility=VISIBLE
        binding.roulette1936Miserbutton.visibility=VISIBLE
        binding.rouletteOkbutton.visibility=VISIBLE
    }

    private fun afficherligne(){
        binding.rouletteLigne1textView.visibility=VISIBLE
        binding.rouletteLigne1NbeditTextNumber.visibility=VISIBLE
        binding.rouletteLigne1Miserbutton.visibility=VISIBLE
        binding.rouletteLigne2textView.visibility=VISIBLE
        binding.rouletteLigne2NbeditTextNumber.visibility=VISIBLE
        binding.rouletteLigne2Miserbutton.visibility=VISIBLE
        binding.rouletteLigne3textView.visibility=VISIBLE
        binding.rouletteLigne3NbeditTextNumber.visibility=VISIBLE
        binding.rouletteLigne3Miserbutton.visibility=VISIBLE
        binding.rouletteOkbutton.visibility=VISIBLE
    }
}