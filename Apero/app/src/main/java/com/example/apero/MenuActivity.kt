package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.apero.databinding.MenuLayoutBinding


class MenuActivity : AppCompatActivity() {

    private lateinit var binding: MenuLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = MenuLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.Colorbutton.setOnClickListener {
            val intent = Intent(this, ColorActivity::class.java)
            startActivity(intent)
        }

        binding.Jonjonbutton.setOnClickListener {
            val intent = Intent(this, JonjonActivity::class.java)
            startActivity(intent)
        }

        binding.Lotobutton.setOnClickListener {
            val intent = Intent(this, LotoActivity::class.java)
            startActivity(intent)
        }

        binding.PileOuFacebutton.setOnClickListener {
            val intent = Intent(this, PileouFaceActivity::class.java)
            startActivity(intent)
        }

        binding.Choixbutton.setOnClickListener {
            val intent = Intent(this, ChoixActivity::class.java)
            startActivity(intent)
        }

        binding.justeprixbutton.setOnClickListener {
            val intent = Intent(this, JustePrixActivity::class.java)
            startActivity(intent)
        }

        binding.PairImpairbutton.setOnClickListener {
            val intent = Intent(this, DePairImpairActivity::class.java)
            startActivity(intent)
        }

        binding.Grillebutton.setOnClickListener {
            val intent = Intent(this, LaGrilleActivity::class.java)
            startActivity(intent)
        }

        binding.WWDbutton.setOnClickListener {
            val intent = Intent(this, WWDActivity::class.java)
            startActivity(intent)
        }

        binding.as4button.setOnClickListener {
            val intent = Intent(this, As4Activity::class.java)
            startActivity(intent)
        }

        binding.Diamantbutton.setOnClickListener {
            val intent = Intent(this, DiamantActivity::class.java)
            startActivity(intent)
        }

        binding.Roulettebutton.setOnClickListener {
            val intent = Intent(this, RouletteActivity::class.java)
            startActivity(intent)
        }
    }
}