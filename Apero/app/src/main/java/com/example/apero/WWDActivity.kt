package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import com.example.apero.databinding.WwdLayoutBinding
import kotlin.random.Random


class WWDActivity : AppCompatActivity() {

    private lateinit var binding: WwdLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = WwdLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.WWDretourbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        binding.WWDnextbutton.visibility = View.INVISIBLE
        binding.WWDboiretextView.visibility= View.INVISIBLE

        var joues=0
        var attaquant=true
        var att=0
        var bourreau=0

        var alreadyplayed=arrayOf(arrayOf(0,0),arrayOf(0,0),arrayOf(0,0),arrayOf(0,0),arrayOf(0,0),arrayOf(0,0))

        var un= arrayOf(0,0)
        var deux= arrayOf(0,0)
        var trois= arrayOf(0,0)
        var quatre= arrayOf(0,0)
        var cinq= arrayOf(0,0)
        var six= arrayOf(0,0)

        var nbj=2
        afficher(nbj,alreadyplayed)

        binding.WWDJ1Hautbutton.setOnClickListener {
            alreadyplayed[0][0]=1
            joues++
            binding.WWDJ1Hautbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=un[0]
                bourreau=1
            }
            else{
                boire(att,un[0],bourreau,1,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ1Basbutton.setOnClickListener {
            alreadyplayed[0][1]=1
            joues++
            binding.WWDJ1Basbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=un[1]
                bourreau=1
            }
            else{
                boire(att,un[1],bourreau,1,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ2Hautbutton.setOnClickListener {
            alreadyplayed[1][0]=1
            joues++
            binding.WWDJ2Hautbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=deux[0]
                bourreau=2
            }
            else{
                boire(att,deux[0],bourreau,2,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ2Basbutton.setOnClickListener {
            alreadyplayed[1][1]=1
            joues++
            binding.WWDJ2Basbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=deux[1]
                bourreau=2
            }
            else{
                boire(att,deux[1],bourreau,2,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ3Hautbutton.setOnClickListener {
            alreadyplayed[2][0]=1
            joues++
            binding.WWDJ3Hautbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=trois[0]
                bourreau=3
            }
            else{
                boire(att,trois[0],bourreau,3,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ3Basbutton.setOnClickListener {
            alreadyplayed[2][1]=1
            joues++
            binding.WWDJ3Basbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=trois[1]
                bourreau=3
            }
            else{
                boire(att,trois[1],bourreau,3,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ4Hautbutton.setOnClickListener {
            alreadyplayed[3][0]=1
            joues++
            binding.WWDJ4Hautbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=quatre[0]
                bourreau=4
            }
            else{
                boire(att,quatre[0],bourreau,4,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ4Basbutton.setOnClickListener {
            alreadyplayed[3][1]=1
            joues++
            binding.WWDJ4Basbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=quatre[1]
                bourreau=4
            }
            else{
                boire(att,quatre[1],bourreau,4,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ5Hautbutton.setOnClickListener {
            alreadyplayed[4][0]=1
            joues++
            binding.WWDJ5Hautbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=cinq[0]
                bourreau=5
            }
            else{
                boire(att,cinq[0],bourreau,5,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ5Basbutton.setOnClickListener {
            alreadyplayed[4][1]=1
            joues++
            binding.WWDJ5Basbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=cinq[1]
                bourreau=5
            }
            else{
                boire(att,cinq[1],bourreau,5,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ6Hautbutton.setOnClickListener {
            alreadyplayed[5][0]=1
            joues++
            binding.WWDJ6Hautbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=six[0]
                bourreau=6
            }
            else{
                boire(att,six[0],bourreau,6,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDJ6Basbutton.setOnClickListener {
            alreadyplayed[5][1]=1
            joues++
            binding.WWDJ6Basbutton.setVisibility(View.INVISIBLE)

            if(attaquant){
                attaquant=false
                att=six[1]
                bourreau=6
            }
            else{
                boire(att,six[1],bourreau,6,alreadyplayed)
                attaquant=true
            }
        }

        binding.WWDnextbutton.setOnClickListener {
            if(joues==nbj*2){
                alreadyplayed=arrayOf(arrayOf(0,0),arrayOf(0,0),arrayOf(0,0),arrayOf(0,0),arrayOf(0,0),arrayOf(0,0))
                binding.WWDlancerbutton.visibility=View.VISIBLE
                joues=0
            }
            afficher(nbj,alreadyplayed)
            binding.WWDnextbutton.setVisibility(View.INVISIBLE)
            binding.WWDboiretextView.setVisibility(View.INVISIBLE)
        }

        binding.WWDnbJeditText.setOnClickListener {
            if (binding.WWDnbJeditText.text.toString().toInt()>=2){
                nbj=binding.WWDnbJeditText.text.toString().toInt()
                alreadyplayed=arrayOf(arrayOf(0,0),arrayOf(0,0),arrayOf(0,0),arrayOf(0,0),arrayOf(0,0),arrayOf(0,0))
                afficher(nbj,alreadyplayed)
                joues=0
            }
        }

        binding.WWDlancerbutton.setOnClickListener {
            binding.WWDlancerbutton.setVisibility(View.INVISIBLE)
            if(nbj==2){
                un=lancer1()
                deux=lancer2()

                binding.WWDJ1Hautbutton.text=String.format("%d",un[0])
                binding.WWDJ1Basbutton.text=String.format("%d",un[1])

                binding.WWDJ2Hautbutton.text=String.format("%d",deux[0])
                binding.WWDJ2Basbutton.text=String.format("%d",deux[1])
            }

            if(nbj==3){
                un=lancer1()
                deux=lancer2()
                trois=lancer3()

                binding.WWDJ1Hautbutton.text=String.format("%d",un[0])
                binding.WWDJ1Basbutton.text=String.format("%d",un[1])

                binding.WWDJ2Hautbutton.text=String.format("%d",deux[0])
                binding.WWDJ2Basbutton.text=String.format("%d",deux[1])

                binding.WWDJ3Hautbutton.text=String.format("%d",trois[0])
                binding.WWDJ3Basbutton.text=String.format("%d",trois[1])
            }

            if(nbj==4){
                un=lancer1()
                deux=lancer2()
                trois=lancer3()
                quatre=lancer4()

                binding.WWDJ1Hautbutton.text=String.format("%d",un[0])
                binding.WWDJ1Basbutton.text=String.format("%d",un[1])

                binding.WWDJ2Hautbutton.text=String.format("%d",deux[0])
                binding.WWDJ2Basbutton.text=String.format("%d",deux[1])

                binding.WWDJ3Hautbutton.text=String.format("%d",trois[0])
                binding.WWDJ3Basbutton.text=String.format("%d",trois[1])

                binding.WWDJ4Hautbutton.text=String.format("%d",quatre[0])
                binding.WWDJ4Basbutton.text=String.format("%d",quatre[1])
            }

            if(nbj==5){
                un=lancer1()
                deux=lancer2()
                trois=lancer3()
                quatre=lancer4()
                cinq=lancer5()

                binding.WWDJ1Hautbutton.text=String.format("%d",un[0])
                binding.WWDJ1Basbutton.text=String.format("%d",un[1])

                binding.WWDJ2Hautbutton.text=String.format("%d",deux[0])
                binding.WWDJ2Basbutton.text=String.format("%d",deux[1])

                binding.WWDJ3Hautbutton.text=String.format("%d",trois[0])
                binding.WWDJ3Basbutton.text=String.format("%d",trois[1])

                binding.WWDJ4Hautbutton.text=String.format("%d",quatre[0])
                binding.WWDJ4Basbutton.text=String.format("%d",quatre[1])

                binding.WWDJ5Hautbutton.text=String.format("%d",cinq[0])
                binding.WWDJ5Basbutton.text=String.format("%d",cinq[1])
            }

            if(nbj==6){
                un=lancer1()
                deux=lancer2()
                trois=lancer3()
                quatre=lancer4()
                cinq=lancer5()
                six=lancer6()

                binding.WWDJ1Hautbutton.text=String.format("%d",un[0])
                binding.WWDJ1Basbutton.text=String.format("%d",un[1])

                binding.WWDJ2Hautbutton.text=String.format("%d",deux[0])
                binding.WWDJ2Basbutton.text=String.format("%d",deux[1])

                binding.WWDJ3Hautbutton.text=String.format("%d",trois[0])
                binding.WWDJ3Basbutton.text=String.format("%d",trois[1])

                binding.WWDJ4Hautbutton.text=String.format("%d",quatre[0])
                binding.WWDJ4Basbutton.text=String.format("%d",quatre[1])

                binding.WWDJ5Hautbutton.text=String.format("%d",cinq[0])
                binding.WWDJ5Basbutton.text=String.format("%d",cinq[1])

                binding.WWDJ6Hautbutton.text=String.format("%d",six[0])
                binding.WWDJ6Basbutton.text=String.format("%d",six[1])
            }
        }
    }

    private fun afficher(nbj:Int,alreadyplayed:Array<Array<Int>>){
        binding.WWDCadreJ1.setVisibility(View.INVISIBLE)
        binding.WWDJ1Basbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ1Hautbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ1textView.setVisibility(View.INVISIBLE)

        binding.WWDCadreJ2.setVisibility(View.INVISIBLE)
        binding.WWDJ2Basbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ2Hautbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ2textView.setVisibility(View.INVISIBLE)

        binding.WWDCadreJ3.setVisibility(View.INVISIBLE)
        binding.WWDJ3Basbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ3Hautbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ3textView.setVisibility(View.INVISIBLE)

        binding.WWDCadreJ4.setVisibility(View.INVISIBLE)
        binding.WWDJ4Basbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ4Hautbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ4textView.setVisibility(View.INVISIBLE)

        binding.WWDCadreJ5.setVisibility(View.INVISIBLE)
        binding.WWDJ5Basbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ5Hautbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ5textView.setVisibility(View.INVISIBLE)

        binding.WWDCadreJ6.setVisibility(View.INVISIBLE)
        binding.WWDJ6Basbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ6Hautbutton.setVisibility(View.INVISIBLE)
        binding.WWDJ6textView.setVisibility(View.INVISIBLE)

        if (nbj==2){
            binding.WWDCadreJ1.setVisibility(View.VISIBLE)
            if(alreadyplayed[0][0]==0){
                binding.WWDJ1Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[0][1]==0){
                binding.WWDJ1Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ1textView.setVisibility(View.VISIBLE)

            binding.WWDCadreJ2.setVisibility(View.VISIBLE)
            if(alreadyplayed[1][0]==0){
                binding.WWDJ2Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[1][1]==0){
                binding.WWDJ2Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ2textView.setVisibility(View.VISIBLE)
        }

        if (nbj==3){
            binding.WWDCadreJ1.setVisibility(View.VISIBLE)
            if(alreadyplayed[0][0]==0){
                binding.WWDJ1Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[0][1]==0){
                binding.WWDJ1Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ1textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ2.setVisibility(View.VISIBLE)
            if(alreadyplayed[1][0]==0){
                binding.WWDJ2Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[1][1]==0){
                binding.WWDJ2Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ2textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ3.setVisibility(View.VISIBLE)
            if(alreadyplayed[2][0]==0){
                binding.WWDJ3Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[2][1]==0){
                binding.WWDJ3Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ3textView.setVisibility(View.VISIBLE)
        }

        if (nbj==4){
            binding.WWDCadreJ1.setVisibility(View.VISIBLE)
            if(alreadyplayed[0][0]==0){
                binding.WWDJ1Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[0][1]==0){
                binding.WWDJ1Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ1textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ2.setVisibility(View.VISIBLE)
            if(alreadyplayed[1][0]==0){
                binding.WWDJ2Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[1][1]==0){
                binding.WWDJ2Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ2textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ3.setVisibility(View.VISIBLE)
            if(alreadyplayed[2][0]==0){
                binding.WWDJ3Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[2][1]==0){
                binding.WWDJ3Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ3textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ4.setVisibility(View.VISIBLE)
            if(alreadyplayed[3][0]==0){
                binding.WWDJ4Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[3][1]==0){
                binding.WWDJ4Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ4textView.setVisibility(View.VISIBLE)
        }

        if (nbj==5){
            binding.WWDCadreJ1.setVisibility(View.VISIBLE)
            if(alreadyplayed[0][0]==0){
                binding.WWDJ1Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[0][1]==0){
                binding.WWDJ1Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ1textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ2.setVisibility(View.VISIBLE)
            if(alreadyplayed[1][0]==0){
                binding.WWDJ2Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[1][1]==0){
                binding.WWDJ2Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ2textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ3.setVisibility(View.VISIBLE)
            if(alreadyplayed[2][0]==0){
                binding.WWDJ3Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[2][1]==0){
                binding.WWDJ3Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ3textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ4.setVisibility(View.VISIBLE)
            if(alreadyplayed[3][0]==0){
                binding.WWDJ4Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[3][1]==0){
                binding.WWDJ4Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ4textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ5.setVisibility(View.VISIBLE)
            if(alreadyplayed[4][0]==0){
                binding.WWDJ5Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[4][1]==0){
                binding.WWDJ5Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ5textView.setVisibility(View.VISIBLE)
        }

        if (nbj==6){
            binding.WWDCadreJ1.setVisibility(View.VISIBLE)
            if(alreadyplayed[0][0]==0){
                binding.WWDJ1Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[0][1]==0){
                binding.WWDJ1Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ1textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ2.setVisibility(View.VISIBLE)
            if(alreadyplayed[1][0]==0){
                binding.WWDJ2Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[1][1]==0){
                binding.WWDJ2Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ2textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ3.setVisibility(View.VISIBLE)
            if(alreadyplayed[2][0]==0){
                binding.WWDJ3Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[2][1]==0){
                binding.WWDJ3Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ3textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ4.setVisibility(View.VISIBLE)
            if(alreadyplayed[3][0]==0){
                binding.WWDJ4Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[3][1]==0){
                binding.WWDJ4Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ4textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ5.setVisibility(View.VISIBLE)
            if(alreadyplayed[4][0]==0){
                binding.WWDJ5Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[4][1]==0){
                binding.WWDJ5Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ5textView.setVisibility(View.VISIBLE)



            binding.WWDCadreJ6.setVisibility(View.VISIBLE)
            if(alreadyplayed[5][0]==0){
                binding.WWDJ6Hautbutton.setVisibility(View.VISIBLE)
            }
            if(alreadyplayed[5][1]==0){
                binding.WWDJ6Basbutton.setVisibility(View.VISIBLE)
            }
            binding.WWDJ6textView.setVisibility(View.VISIBLE)
        }
    }

    private fun lancer1():Array<Int>{
        return(arrayOf(Random.nextInt(1,7),Random.nextInt(1,7)))
    }

    private fun lancer2():Array<Int>{
        return(arrayOf(Random.nextInt(1,7),Random.nextInt(1,7)))
    }

    private fun lancer3():Array<Int>{
        return(arrayOf(Random.nextInt(1,7),Random.nextInt(1,7)))
    }

    private fun lancer4():Array<Int>{
        return(arrayOf(Random.nextInt(1,7),Random.nextInt(1,7)))
    }

    private fun lancer5():Array<Int>{
        return(arrayOf(Random.nextInt(1,7),Random.nextInt(1,7)))
    }

    private fun lancer6():Array<Int>{
        return(arrayOf(Random.nextInt(1,7),Random.nextInt(1,7)))
    }

    private fun boire(att:Int,def:Int,bourreau:Int,victime:Int,alreadyplayed:Array<Array<Int>>){
        afficher(0,alreadyplayed)
        binding.WWDnextbutton.setVisibility(View.VISIBLE)
        binding.WWDboiretextView.setVisibility(View.VISIBLE)

        if(att>=def){
            binding.WWDboiretextView.text=String.format("Joueur %d boit %d",victime,att-def)
        }
        else{
            binding.WWDboiretextView.text=String.format("Joueur %d boit %d",bourreau,def-att)
        }
    }
}