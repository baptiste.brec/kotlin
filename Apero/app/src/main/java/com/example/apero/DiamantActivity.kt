package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import com.example.apero.databinding.DiamantLayoutBinding
import kotlin.random.Random


class DiamantActivity : AppCompatActivity() {

    private lateinit var binding: DiamantLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DiamantLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.diamantretourbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        binding.diamantresetbutton.setOnClickListener {
            val intent = Intent(this, DiamantActivity::class.java)
            startActivity(intent)
        }

        depart()
        var nbj=2
        var listjoueurs= mutableListOf<Int>()
        //var ok=true
        var manche=1
        var artefacts=0
        var artefactstable=0
        val deck= mutableListOf("1 \uD83D\uDC8E","2 \uD83D\uDC8E","3 \uD83D\uDC8E","4 \uD83D\uDC8E","5 \uD83D\uDC8E","5 \uD83D\uDC8E","7 \uD83D\uDC8E","7 \uD83D\uDC8E","9 \uD83D\uDC8E","11 \uD83D\uDC8E","11 \uD83D\uDC8E","13 \uD83D\uDC8E","14 \uD83D\uDC8E","15 \uD83D\uDC8E","\uD83D\uDC7F","\uD83D\uDC7F","\uD83D\uDC7F","\uD83D\uDC7A","\uD83D\uDC7A","\uD83D\uDC7A","☠","☠","☠","\uD83D\uDC7D","\uD83D\uDC7D","\uD83D\uDC7D","\uD83D\uDCA9","\uD83D\uDCA9","\uD83D\uDCA9","\uD83E\uDD43","\uD83E\uDD42","\uD83C\uDF7B","\uD83C\uDF7A","\uD83C\uDF79")
        var joueur=quijoue(listjoueurs)
        var joueurrestant=0
        var diamanttable=0


        binding.DiamantJoueurtextView.text=String.format("Nombre de joueurs:\n%d",nbj)

        binding.diamantplusbutton.setOnClickListener {
            nbj++
            binding.DiamantJoueurtextView.text=String.format("Nombre de joueurs:\n%d",nbj)
        }

        binding.diamantmoinsbutton.setOnClickListener {
            if(nbj>2){
                nbj--
                binding.DiamantJoueurtextView.text=String.format("Nombre de joueurs:\n%d",nbj)
            }
        }

        binding.diamantvalidernbjbutton.setOnClickListener {
            cacherjoueurs(manche,diamanttable)
            listjoueurs=listedesjoueurs(nbj,listjoueurs)
            binding.DiamantJoueurtextView.text=String.format("%d artefacts\nsortis",artefacts)
            joueur=quijoue(listjoueurs)
            joueurrestant=nbj
        }





        binding.diamantPiocherbutton.setOnClickListener {
            val nbcard=Random.nextInt(deck.size)

            binding.diamantcartetextView.text=String.format("%s",deck[nbcard])
            binding.diamantPiocherbutton.visibility=View.INVISIBLE
            binding.diamantChoisirbutton.visibility=View.VISIBLE

            if (piocher(deck[nbcard])=="Diamant"){
                binding.diamantdiamantparpersonnetextView.text=String.format("+%d \uD83D\uDC8E\npar joueur restant",piocherdiamant(deck[nbcard])/joueurrestant)
                diamanttable+=piocherdiamant(deck[nbcard])%joueurrestant
                binding.DiamantRestetextView.text=String.format("%d Diamants\nsur la table",diamanttable)
            }
            else{
                if (piocher(deck[nbcard])=="Piege"){
                    if (piocherpiege(deck[nbcard],manche)!=manche){

                        findemanche()
                        diamanttable=0
                        artefactstable=0
                        binding.DiamantnbartefactstabletextView.text=String.format("%d Artefacts\nsur la table",artefactstable)
                        listjoueurs.clear()
                        listedesjoueurs(nbj,listjoueurs)
                        manche++
                        if (manche>5){
                            endgame()
                            cacherjoueurs(manche,diamanttable)
                        }
                        else
                            cacherjoueurs(manche,diamanttable)
                    }
                }
                else{
                    artefacts++
                    artefactstable++
                    binding.DiamantJoueurtextView.text=String.format("%d artefacts\nsortis",artefacts)
                    binding.DiamantnbartefactstabletextView.text=String.format("%d Artefacts\nsur la table",artefactstable)
                    piocherartefact(piocher(deck[nbcard]))
                }
            }
            deck.removeAt(nbcard)
        }

        binding.diamantChoisirbutton.setOnClickListener {
            binding.diamantsuivantbutton.visibility=View.VISIBLE
            binding.diamantChoisirbutton.visibility=View.INVISIBLE
            binding.diamantdiamantparpersonnetextView.visibility= View.INVISIBLE
            binding.diamantsuivantbutton.text=String.format("Ok")

            binding.diamantqueljoueurtextView.visibility= View.VISIBLE

            joueur=quijoue(listjoueurs)
            binding.diamantqueljoueurtextView.text=String.format("Joueur %d",joueur)
        }

        binding.diamantsuivantbutton.setOnClickListener {
            binding.diamantsuivantbutton.visibility=View.INVISIBLE
            binding.diamantresterbutton.visibility=View.VISIBLE
            binding.diamantarreterbutton.visibility=View.VISIBLE

            binding.diamantnbartefactsJtextView.visibility= View.VISIBLE
            binding.diamantnbdiamantJtextView.visibility= View.VISIBLE
        }

        binding.diamantresterbutton.setOnClickListener {
            binding.diamantnbartefactsJtextView.visibility= View.INVISIBLE
            binding.diamantnbdiamantJtextView.visibility= View.INVISIBLE

            listjoueurs[joueur-1]=0
            joueur++
            joueur=quijoue(listjoueurs)
            if (joueur<=nbj && joueur!=0){
                binding.diamantqueljoueurtextView.text=String.format("Joueur %d",joueur)
                binding.diamantsuivantbutton.visibility=View.VISIBLE
                binding.diamantresterbutton.visibility=View.INVISIBLE
                binding.diamantarreterbutton.visibility=View.INVISIBLE
            }
            else{
                val changermanche=remettreliste(listjoueurs)
                if (changermanche==1){
                    //fin de manche
                    findemanche()
                    diamanttable=0
                    artefactstable=0
                    binding.DiamantnbartefactstabletextView.text=String.format("%d Artefact\nsur la table",artefactstable)
                    listjoueurs.clear()
                    listedesjoueurs(nbj,listjoueurs)
                    manche++
                    if (manche>5){
                        endgame()
                        cacherjoueurs(manche,diamanttable)
                    }
                    else
                        cacherjoueurs(manche,diamanttable)
                }
                else{
                    cacherjoueurs(manche,diamanttable)
                }
                joueur=quijoue(listjoueurs)
            }
        }

        binding.diamantarreterbutton.setOnClickListener {
            binding.diamantnbartefactsJtextView.visibility= View.INVISIBLE
            binding.diamantnbdiamantJtextView.visibility= View.INVISIBLE

            joueurrestant--
            listjoueurs[joueur-1]=2
            joueur++
            joueur=quijoue(listjoueurs)
            if (joueur<=nbj && joueur!=0){
                binding.diamantqueljoueurtextView.text=String.format("Joueur %d",joueur)
                binding.diamantsuivantbutton.visibility=View.VISIBLE
                binding.diamantresterbutton.visibility=View.INVISIBLE
                binding.diamantarreterbutton.visibility=View.INVISIBLE
            }
            else{
                val changermanche=remettreliste(listjoueurs)
                if (changermanche==1){
                    //fin de manche
                    findemanche()
                    diamanttable=0
                    artefactstable=0
                    binding.DiamantnbartefactstabletextView.text=String.format("%d Artefact\nsur la table",artefactstable)
                    joueurrestant=nbj
                    listjoueurs.clear()
                    listedesjoueurs(nbj,listjoueurs)
                    manche++
                    if (manche>5){
                        endgame()
                        cacherjoueurs(manche,diamanttable)
                    }
                    else
                        cacherjoueurs(manche,diamanttable)
                }
                else{
                    cacherjoueurs(manche,diamanttable)
                }
                joueur=quijoue(listjoueurs)
            }
        }
    }

    private fun piocher(carte:String):String{
        if (carte=="1 \uD83D\uDC8E" || carte=="2 \uD83D\uDC8E"|| carte=="3 \uD83D\uDC8E"|| carte=="4 \uD83D\uDC8E"|| carte=="5 \uD83D\uDC8E"|| carte=="5 \uD83D\uDC8E"|| carte=="7 \uD83D\uDC8E"|| carte=="7 \uD83D\uDC8E"|| carte=="9 \uD83D\uDC8E"|| carte=="11 \uD83D\uDC8E"|| carte=="11 \uD83D\uDC8E"|| carte=="13 \uD83D\uDC8E"|| carte=="14 \uD83D\uDC8E"|| carte=="15 \uD83D\uDC8E")
            return("Diamant")

        if (carte=="\uD83D\uDC7F" || carte=="👺"|| carte=="☠"|| carte=="👽"|| carte=="💩")
            return("Piege")
        return("Artefact")
    }

    private fun piocherpiege(carte:String,manche:Int):Int{
        if(carte=="\uD83D\uDC7F"){
            if (binding.diamantpiegediablenbtextView.text=="0")
                binding.diamantpiegediablenbtextView.text="1"
            else{
                binding.diamantpiegediablenbtextView.text="2"
                findemanche()
                return(ajoutermanche(manche))
            }
        }
        else{
            if(carte=="\uD83D\uDC7A"){
                if (binding.diamantpiegedemonnbtextView.text=="0")
                    binding.diamantpiegedemonnbtextView.text="1"
                else{
                    binding.diamantpiegedemonnbtextView.text="2"
                    findemanche()
                    return(ajoutermanche(manche))
                }
            }
            else{
                if (carte=="☠"){
                    if (binding.diamantpiegemortnbtextView.text=="0")
                        binding.diamantpiegemortnbtextView.text="1"
                    else{
                        binding.diamantpiegemortnbtextView.text="2"
                        findemanche()
                        return(ajoutermanche(manche))
                    }
                }
                else{
                    if (carte=="\uD83D\uDC7D"){
                        if (binding.diamantpiegeETnbtextView.text=="0")
                            binding.diamantpiegeETnbtextView.text="1"
                        else{
                            binding.diamantpiegeETnbtextView.text="2"
                            findemanche()
                            return(ajoutermanche(manche))
                        }
                    }
                    else{
                        if (binding.diamantpiegecacanbtextView.text=="0")
                            binding.diamantpiegecacanbtextView.text="1"
                        else{
                            binding.diamantpiegecacanbtextView.text="2"
                            findemanche()
                            return(ajoutermanche(manche))
                        }
                    }
                }
            }
        }
        return(manche)
    }

    private fun piocherdiamant(carte:String):Int{
        binding.diamantdiamantparpersonnetextView.visibility= View.VISIBLE
        if (carte=="1 \uD83D\uDC8E"){
            return(1)
        }
        if (carte=="2 \uD83D\uDC8E"){
            return(2)
        }
        if (carte=="3 \uD83D\uDC8E"){
            return(3)
        }
        if (carte=="4 \uD83D\uDC8E"){
            return(4)
        }
        if (carte=="5 \uD83D\uDC8E"){
            return(5)
        }
        if (carte=="7 \uD83D\uDC8E"){
            return(7)
        }
        if (carte=="9 \uD83D\uDC8E"){
            return(9)
        }
        if (carte=="11 \uD83D\uDC8E"){
            return(11)
        }
        if (carte=="13 \uD83D\uDC8E"){
            return(13)
        }
        if (carte=="14 \uD83D\uDC8E"){
            return(14)
        }
        return(15)
    }

    private fun piocherartefact(carte:String):Int{
        return(0)
    }

    private fun ajoutermanche(manche:Int):Int{
        return(manche+1)
    }

    private fun findemanche(){
        //binding.diamantdiamanttabletextView.text=String.format("Fin de manche.")

        binding.diamantpiegediablenbtextView.text=String.format("0")
        binding.diamantpiegedemonnbtextView.text=String.format("0")
        binding.diamantpiegemortnbtextView.text=String.format("0")
        binding.diamantpiegeETnbtextView.text=String.format("0")
        binding.diamantpiegecacanbtextView.text=String.format("0")
    }

    private fun endgame(){
        binding.diamantresultatsbutton.visibility= View.VISIBLE

        binding.diamantcartetextView.visibility= View.INVISIBLE
        binding.DiamantJoueurtextView.visibility= View.INVISIBLE
        binding.diamantdiamanttabletextView.visibility= View.INVISIBLE
        binding.diamantsuivantbutton.visibility= View.INVISIBLE
        binding.diamantcartetextView.visibility= View.INVISIBLE
        binding.diamantnbdiamantJtextView.visibility= View.INVISIBLE
        binding.diamantnbartefactsJtextView.visibility= View.INVISIBLE


        binding.diamantqueljoueurtextView.text=String.format("Fin de partie.")

        binding.diamantresultatsbutton.setOnClickListener {
            val intent = Intent(this, As4Activity::class.java)
            startActivity(intent)
        }
    }

    private fun remettreliste(liste:MutableList<Int>):Int{
        var tousjoues=false
        for (i in liste){
            if (i!=2){
               tousjoues=true
            }
        }
        if(tousjoues){
            for (i in 0 until liste.size){
                if (liste[i]==0)
                    liste[i]=1
            }
            return(0)
        }
        else{
            findemanche()
            return(1)
        }
    }

    private fun quijoue(liste: MutableList<Int>):Int{
        for (i in 0 until liste.size){
            if(liste[i]==1){
                return(i+1)
            }
        }
        return(0)
    }

    private  fun listedesjoueurs(nbj:Int,liste: MutableList<Int>):MutableList<Int>{
        for (i in 1..nbj)
            liste.add(1)
        return(liste)
    }

    private fun depart(){
        binding.DiamantnbartefactstabletextView.visibility= View.INVISIBLE
        binding.DiamantRestetextView.visibility= View.INVISIBLE

        binding.diamantpiegediabletextView.visibility= View.INVISIBLE
        binding.diamantpiegedemontextView.visibility= View.INVISIBLE
        binding.diamantpiegemorttextView.visibility= View.INVISIBLE
        binding.diamantpiegeETtextView.visibility= View.INVISIBLE
        binding.diamantpiegecacatextView.visibility= View.INVISIBLE

        binding.diamantpiegediablenbtextView.visibility= View.INVISIBLE
        binding.diamantpiegedemonnbtextView.visibility= View.INVISIBLE
        binding.diamantpiegemortnbtextView.visibility= View.INVISIBLE
        binding.diamantpiegeETnbtextView.visibility= View.INVISIBLE
        binding.diamantpiegecacanbtextView.visibility= View.INVISIBLE

        binding.diamantcadreimageView.visibility= View.INVISIBLE
        binding.diamantcartetextView.visibility= View.INVISIBLE

        binding.diamantresterbutton.visibility= View.INVISIBLE
        binding.diamantarreterbutton.visibility= View.INVISIBLE
        binding.diamantPiocherbutton.visibility= View.INVISIBLE
        binding.diamantChoisirbutton.visibility= View.INVISIBLE
        binding.diamantsuivantbutton.visibility= View.INVISIBLE

        binding.diamantnbartefactsJtextView.visibility= View.INVISIBLE
        binding.diamantnbdiamantJtextView.visibility= View.INVISIBLE

        binding.diamantdiamanttabletextView.visibility= View.INVISIBLE
        binding.diamantmanchetextView.visibility= View.INVISIBLE

        binding.diamantqueljoueurtextView.visibility= View.INVISIBLE


        binding.diamantresultatsbutton.visibility= View.INVISIBLE
        binding.diamantdiamantparpersonnetextView.visibility= View.INVISIBLE
    }

    private fun cacherjoueurs(manche:Int,diamanttable:Int){
        binding.diamantmoinsbutton.visibility= View.INVISIBLE
        binding.diamantplusbutton.visibility= View.INVISIBLE
        binding.diamantvalidernbjbutton.visibility= View.INVISIBLE

        binding.diamantsuivantbutton.visibility= View.INVISIBLE
        binding.diamantnbartefactsJtextView.visibility= View.INVISIBLE
        binding.diamantnbdiamantJtextView.visibility= View.INVISIBLE

        binding.diamantqueljoueurtextView.visibility= View.INVISIBLE


        binding.DiamantnbartefactstabletextView.visibility= View.VISIBLE
        binding.DiamantRestetextView.visibility= View.VISIBLE

        binding.diamantpiegediabletextView.visibility= View.VISIBLE
        binding.diamantpiegedemontextView.visibility= View.VISIBLE
        binding.diamantpiegemorttextView.visibility= View.VISIBLE
        binding.diamantpiegeETtextView.visibility= View.VISIBLE
        binding.diamantpiegecacatextView.visibility= View.VISIBLE

        binding.diamantpiegediablenbtextView.visibility= View.VISIBLE
        binding.diamantpiegedemonnbtextView.visibility= View.VISIBLE
        binding.diamantpiegemortnbtextView.visibility= View.VISIBLE
        binding.diamantpiegeETnbtextView.visibility= View.VISIBLE
        binding.diamantpiegecacanbtextView.visibility= View.VISIBLE

        binding.diamantcadreimageView.visibility= View.VISIBLE
        binding.diamantcartetextView.visibility= View.VISIBLE

        binding.diamantPiocherbutton.visibility= View.VISIBLE

        binding.diamantmanchetextView.visibility= View.VISIBLE
        binding.diamantmanchetextView.text=String.format("Manche\n%d/5",manche)

        binding.diamantdiamanttabletextView.visibility= View.VISIBLE
        binding.DiamantRestetextView.text=String.format("%d Diamants\nsur la table",diamanttable)

        binding.diamantsuivantbutton.visibility= View.INVISIBLE
        binding.diamantChoisirbutton.visibility= View.INVISIBLE
        binding.diamantresterbutton.visibility=View.INVISIBLE
        binding.diamantarreterbutton.visibility=View.INVISIBLE

        binding.diamantdiamantparpersonnetextView.visibility= View.INVISIBLE
    }
}