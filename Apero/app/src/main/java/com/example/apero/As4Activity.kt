package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import com.example.apero.databinding.As4LayoutBinding
import kotlin.random.Random


class As4Activity : AppCompatActivity() {

    private lateinit var binding: As4LayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = As4LayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.as4retourbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        var nbas=4
        var deck= mutableListOf("As ♠","2 ♠","3 ♠️","4 ♠️","5 ♠","6 ♠","7 ♠","8 ♠","9 ♠","10 ♠","V ♠","D ♠","R ♠","As ♣","2 ♣","3 ♣️","4 ♣️","5 ♣","6 ♣","7 ♣","8 ♣","9 ♣","10 ♣","V ♣","D ♣","R ♣","As ❤","2 ❤","3 ❤️","4 ❤️","5 ❤","6 ❤","7 ❤","8 ❤","9 ❤","10 ❤","V ❤","D ❤","R ❤","As ♦","2 ♦","3 ♦️","4 ♦️","5 ♦","6 ♦","7 ♦","8 ♦","9 ♦","10 ♦","V ♦","D ♦","R ♦","Joker")
        afficherreste(deck,nbas)

        binding.as4resetbutton.setOnClickListener {
            nbas=4
            deck= mutableListOf("As ♠","2 ♠","3 ♠️","4 ♠️","5 ♠","6 ♠","7 ♠","8 ♠","9 ♠","10 ♠","V ♠","D ♠","R ♠","As ♣","2 ♣","3 ♣️","4 ♣️","5 ♣","6 ♣","7 ♣","8 ♣","9 ♣","10 ♣","V ♣","D ♣","R ♣","As ❤","2 ❤","3 ❤️","4 ❤️","5 ❤","6 ❤","7 ❤","8 ❤","9 ❤","10 ❤","V ❤","D ❤","R ❤","As ♦","2 ♦","3 ♦️","4 ♦️","5 ♦","6 ♦","7 ♦","8 ♦","9 ♦","10 ♦","V ♦","D ♦","R ♦","Joker")
            afficherreste(deck,nbas)
            binding.as4piocherbutton.visibility= View.VISIBLE
            binding.as4affichertextView.text=String.format("Piochez")
        }

        binding.as4piocherbutton.setOnClickListener {
            if(deck.size!=0){
                val carte=deck[Random.nextInt(deck.size)]
                deck.remove(carte)
                if(carte=="As ♠" || carte =="As ♣" || carte =="As ❤" || carte == "As ♦"){
                    nbas--
                    piocheras(carte,nbas)
                }
                else
                    piocher(carte)
                afficherreste(deck,nbas)
            }
            else{
                binding.as4piocherbutton.visibility= View.INVISIBLE
                binding.as4affichertextView.text=String.format("Toutes les cartes ont été piochées")
            }
        }
    }

    private fun piocheras(carte:String,nbas:Int){
        if (nbas==3)
            binding.as4affichertextView.text=String.format("%s\n\nChoisis le récipient.",carte)
        else{
            if (nbas==2)
                binding.as4affichertextView.text=String.format("%s\n\nChoisis tout ce qu'on mettra dedans.",carte)
            else{
                if(nbas==1)
                    binding.as4affichertextView.text=String.format("%s\n\nChoisis les quantités",carte)
                else
                    binding.as4affichertextView.text=String.format("%s\n\nBois",carte)
            }
        }
    }

    private fun piocher(carte:String){
        binding.as4affichertextView.text=String.format("%s",carte)
    }

    private fun afficherreste(deck:MutableList<String>,nbas:Int){
        binding.as4txtcardtextView.text=String.format("Il reste %d cartes",deck.size)
        binding.as4txtnbastextView.text=String.format("Il reste %d as",nbas)
    }
}