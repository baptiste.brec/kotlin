package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import com.example.apero.databinding.ChoixLayoutBinding
import kotlin.random.Random


class ChoixActivity : AppCompatActivity() {

    private lateinit var binding: ChoixLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ChoixLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val listesoft: MutableList<String> = mutableListOf("Boire 1 gorgée","Boire 2 gorgées","Boire 3 gorgées","Donner 1 gorgée","Donner 2 gorgées","Donner 3 gorgées","Boire un verre d'eau","Tout le monde boit une gorgée","Tous les autres boivent une gorgée","Boire une gorgée dans le verre de tout le monde")
        val listehard: MutableList<String> = mutableListOf("Boire 1 gorgée","Boire 2 gorgées","Boire 3 gorgées","Boire 4 gorgées","Boire 5 gorgées","Donner 1 gorgée","Donner 2 gorgées","Donner 3 gorgées","Donner 4 gorgées","Donner 5 gorgées","Faire un cul sec","Tout le monde fait un cul sec","Faire un cul sec avec les verres de tout le monde","Laisser ton voisin de droite remplir ton verre","Boire 1 shooter","Boire 2 shooters","Donner 1 shooter","Donner 2 shooters","Donner un cul sec","Tout le monde boit 2 gorgées")

        var hard= false
        binding.Choixfixebutton.text=String.format("Boire 1 gorgée")


        binding.Choixsuivantbutton.setVisibility(View.INVISIBLE)
        binding.ChoixChoisittextView.setVisibility(View.INVISIBLE)

        binding.Choixretourbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        binding.ChoixHardswitch.setOnClickListener{
            hard = binding.ChoixHardswitch.isChecked
            if (hard){
                binding.Choixfixebutton.text=String.format("Boire 3 gorgées")
            }
            else{
                binding.Choixfixebutton.text=String.format("Boire 1 gorgée")
            }
        }

        binding.Choixfixebutton.setOnClickListener {
            binding.ChoixentetetextView.setVisibility(View.INVISIBLE)
            binding.Choixfixebutton.setVisibility(View.INVISIBLE)
            binding.Choixquestionbutton.setVisibility(View.INVISIBLE)
            binding.ChoixmidtextView.setVisibility(View.INVISIBLE)

            binding.Choixsuivantbutton.setVisibility(View.VISIBLE)
            binding.ChoixChoisittextView.setVisibility(View.VISIBLE)

            if (hard){
                binding.ChoixChoisittextView.text=String.format("Boire 3 gorgées")
            }
            else{
                binding.ChoixChoisittextView.text=String.format("Boire 1 gorgée")
            }
        }

        binding.Choixquestionbutton.setOnClickListener {
            binding.ChoixentetetextView.setVisibility(View.INVISIBLE)
            binding.Choixfixebutton.setVisibility(View.INVISIBLE)
            binding.Choixquestionbutton.setVisibility(View.INVISIBLE)
            binding.ChoixmidtextView.setVisibility(View.INVISIBLE)

            binding.Choixsuivantbutton.setVisibility(View.VISIBLE)
            binding.ChoixChoisittextView.setVisibility(View.VISIBLE)

            if(hard){
                afficherchoix(listehard)
            }
            else{
                afficherchoix(listesoft)
            }
        }

        binding.Choixsuivantbutton.setOnClickListener {
            binding.ChoixentetetextView.setVisibility(View.VISIBLE)
            binding.Choixfixebutton.setVisibility(View.VISIBLE)
            binding.Choixquestionbutton.setVisibility(View.VISIBLE)
            binding.ChoixmidtextView.setVisibility(View.VISIBLE)

            binding.Choixsuivantbutton.setVisibility(View.INVISIBLE)
            binding.ChoixChoisittextView.setVisibility(View.INVISIBLE)
        }
    }

    private fun afficherchoix(liste:MutableList<String>){
        binding.ChoixChoisittextView.text=String.format("%s", liste[Random.nextInt(0,liste.size)])
    }
}