package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.view.View

import com.example.apero.databinding.JusteprixLayoutBinding
import kotlin.random.Random


class JustePrixActivity : AppCompatActivity() {

    private lateinit var binding: JusteprixLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = JusteprixLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.justeprixretourbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        var max=20
        binding.justeprixchoisirintervalText.hint=String.format("%d",max)

        var rdm= Random.nextInt(1,max+1)
        var compteur=0

        binding.justeprixmaxbutton.setOnClickListener {
            max =binding.justeprixchoisirintervalText.text.toString().toInt()
            binding.justeprixchoisirintervalText.text.clear()
            binding.justeprixchoisirintervalText.hint=String.format("%d",max)
            rdm= Random.nextInt(1,max+1)
        }

        binding.justeprixvaliderbutton.setOnClickListener {
            compteur++
            binding.justeprixcompteurtextView.text=String.format("%d",compteur)
            if(Resultat(binding.justeprixpropositionText.text.toString().toInt(),rdm)){
                if(compteur>1){
                    binding.justeprixaffichertextView.text=String.format("Gagné, vous devez boire %d gorgées",compteur)
                }
                else{
                    binding.justeprixaffichertextView.text=String.format("TOUT PILE, les autres finissent leur verre")
                }
            }
        }

        binding.justeprixresetbutton.setOnClickListener {
            rdm= Random.nextInt(1,max+1)
            compteur=0
            binding.justeprixaffichertextView.text=String.format("Indications")
            binding.justeprixcompteurtextView.text=String.format("%d",compteur)
        }
    }

    private fun Resultat(propal: Int,random: Int):Boolean{
        if (propal<random){
            binding.justeprixaffichertextView.text=String.format("Plus grand")
            return(false)
        }
        if (random<propal){
            binding.justeprixaffichertextView.text=String.format("Plus petit")
            return(false)
        }
        return(true)
    }

}