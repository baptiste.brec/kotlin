package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.graphics.toColorInt
import com.example.apero.databinding.ColorLayoutBinding
import kotlin.random.Random
import android.graphics.Color


class ColorActivity : AppCompatActivity() {
    private lateinit var binding: ColorLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ColorLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val color: MutableList<String> = mutableListOf("Bleu","Gris","Marron","Orange","Rouge","Violet","Blanc","Jaune","Noir","Rose","Vert")
        val codecolor: MutableList<Int> = mutableListOf((4278193663/2)*2,(4282531390/2)*2,(2152926734/2)*2,(4294931968/2)*2,(4294901760/2)*2,(4284616932/2)*2,(4294960640/2)*2,(4294902002/2)*2,(4278255385/2)*2)
        val liaison: MutableList<String> = mutableListOf("Et du","Mais sans","Ou du")



        binding.retourcolorbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        binding.lancerjonjonbutton.setOnClickListener {
            var nbcolor= Random.nextInt(1,4)
            if (nbcolor==1){
                color1(color,codecolor)
            }
            else{
                if (nbcolor==2){
                    color2(color,liaison,codecolor)
                }
                else{
                    color3(color,codecolor)
                }
            }
        }
    }

    private fun color1(Color:MutableList<String>,CodeColor:MutableList<Int>){
        binding.textViewtop.setVisibility(View.GONE)
        binding.textViewbot.setVisibility(View.GONE)

        binding.textViewmid.text=String.format("%s",Color[Random.nextInt(0,Color.size)])
        binding.textViewmid.setTextColor(CodeColor[Random.nextInt(0,CodeColor.size)])
    }

    private fun color2(Color:MutableList<String>,Liaison:MutableList<String>,CodeColor:MutableList<Int>){
        binding.textViewtop.setVisibility(View.VISIBLE)
        binding.textViewbot.setVisibility(View.VISIBLE)

        var rdtop=Random.nextInt(0,Color.size)
        var rdbot=Random.nextInt(0,Color.size)

        while(rdtop==rdbot){
            rdbot=Random.nextInt(0,Color.size)
        }

        binding.textViewtop.text=String.format("%s",Color[rdtop])
        binding.textViewtop.setTextColor(CodeColor[Random.nextInt(0,CodeColor.size)])
        binding.textViewbot.text=String.format("%s",Color[rdbot])
        binding.textViewbot.setTextColor(CodeColor[Random.nextInt(0,CodeColor.size)])
        binding.textViewmid.text=String.format("%s",Liaison[Random.nextInt(0,Liaison.size)])
        binding.textViewmid.setTextColor((4278190080/2)*2)
    }

    private fun color3(Color:MutableList<String>,CodeColor:MutableList<Int>){
        binding.textViewtop.setVisibility(View.VISIBLE)
        binding.textViewbot.setVisibility(View.VISIBLE)

        var rdtop=Random.nextInt(0,Color.size)
        var rdmid=Random.nextInt(0,Color.size)

        while(rdtop==rdmid){
            rdmid=Random.nextInt(0,Color.size)
        }

        var rdbot=Random.nextInt(0,Color.size)

        while(rdbot==rdtop || rdbot==rdmid){
            rdbot=Random.nextInt(0,Color.size)
        }

        var rd1ou2top=Random.nextInt(1,3)

        if (rd1ou2top==1){
            binding.textViewtop.text=String.format("%s",Color[rdtop])
            binding.textViewtop.setTextColor(CodeColor[Random.nextInt(0,CodeColor.size)])
            binding.textViewbot.text=String.format("%s ni du %s",Color[rdbot],Color[rdmid])
            binding.textViewbot.setTextColor(CodeColor[Random.nextInt(0,CodeColor.size)])
            binding.textViewmid.text=String.format("Mais sans")
            binding.textViewmid.setTextColor((4278190080/2)*2)
        }
        else{
            binding.textViewtop.text=String.format("%s et du %s",Color[rdtop],Color[rdmid])
            binding.textViewtop.setTextColor(CodeColor[Random.nextInt(0,CodeColor.size)])
            binding.textViewbot.text=String.format("%s",Color[rdbot])
            binding.textViewbot.setTextColor(CodeColor[Random.nextInt(0,CodeColor.size)])
            binding.textViewmid.text=String.format("Mais sans")
            binding.textViewmid.setTextColor((4278190080/2)*2)
        }
    }
}
