package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.example.apero.databinding.PileoufaceLayoutBinding
import kotlin.random.Random


class PileouFaceActivity : AppCompatActivity() {

    private lateinit var binding: PileoufaceLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = PileoufaceLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        var previous=10
        var NB=0


        binding.POFretourbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        binding.POFLancerbutton.setOnClickListener {
            var current=Random.nextInt(2)
            NB++

            if (current==0){
                binding.POFaffichertextView.text=String.format("PILE")
            }
            else{
                binding.POFaffichertextView.text=String.format("Face")
            }

            if (current==previous || previous==10){
                binding.POFNbtextView.text=String.format("%d",NB)
            }
            else{
                binding.POFNbtextView.text=String.format("Perdu! Tu bois: %d",NB-1)
                NB=1
            }
            previous=current
        }

    }
}