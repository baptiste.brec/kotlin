package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.example.apero.databinding.DepairimpairLayoutBinding
import kotlin.random.Random


class DePairImpairActivity : AppCompatActivity() {

    private lateinit var binding: DepairimpairLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DepairimpairLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.DPIretourbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        var changer=false
        var nbd=1

        binding.DPInbdeseditText.setOnClickListener {
            changer=true
        }

        binding.DPIlancerbutton.setOnClickListener {
            if (changer){
                var rdm=Random.nextInt(1,(binding.DPInbdeseditText.text.toString().toInt()*6)+1)
                binding.DPIafficherdetextView.text=String.format("%d",rdm)
                if (rdm%2==0){
                    binding.DPIquiboittextView.text=String.format("Tu distribues")
                }
                else{
                    binding.DPIquiboittextView.text=String.format("Tu bois")
                }
            }
            else{
                var rdm=Random.nextInt(1,(nbd*6)+1)
                binding.DPIafficherdetextView.text=String.format("%d",rdm)
                if (rdm%2==0){
                    binding.DPIquiboittextView.text=String.format("Tu distribues")
                }
                else{
                    binding.DPIquiboittextView.text=String.format("Tu bois")
                }
            }
        }
    }
}