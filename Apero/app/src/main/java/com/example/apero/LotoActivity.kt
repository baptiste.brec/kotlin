package com.example.apero

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.content.Intent

import com.example.apero.databinding.LotoLayoutBinding
import kotlin.random.Random


class LotoActivity : AppCompatActivity() {

    private lateinit var binding: LotoLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = LotoLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val restants: MutableList<Int> = mutableListOf()

        creerliste(restants)




        binding.Lotoretourbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        binding.LotoLancerbutton.setOnClickListener {
            if(restants.size==0){
                binding.LotoNbtextView.text=String.format("Tous les numéros sont sortis")
            }
            else{
                var tirer= Random.nextInt(0,restants.size)
                binding.LotoNbtextView.text=String.format("%d",restants[tirer])
                restants.removeAt(tirer)
            }
        }

        binding.Lotoresetbutton.setOnClickListener {
            binding.LotoNbtextView.text=String.format("0")
            restants.removeAll(restants)
            creerliste(restants)
        }

    }

    private fun creerliste(liste:MutableList<Int>){
        for (i in 1..90){
            liste.add(i)
        }
    }
}