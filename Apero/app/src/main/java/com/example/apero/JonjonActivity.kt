package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.example.apero.databinding.JonjonLayoutBinding
import kotlin.random.Random


class JonjonActivity : AppCompatActivity() {

    private lateinit var binding: JonjonLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = JonjonLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        var nbpl=1
        Updatetxtply(nbpl)


        binding.moinsjonjonbutton.setOnClickListener {
            if(nbpl>1)
                nbpl--
            Updatetxtply(nbpl)
        }

        binding.plusjonjonbutton.setOnClickListener {
            nbpl++
            Updatetxtply(nbpl)
        }

        binding.retourjonjonbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        binding.Jouerjonjonbutton.setOnClickListener {
            binding.Jonjonaffichertext.text=String.format("%d doigts", Random.nextInt(1,(nbpl*2)+1))
        }
    }

    private fun Updatetxtply(nbpl:Int){
        binding.affichernbpltext.text=String.format("%d",nbpl)
    }
}