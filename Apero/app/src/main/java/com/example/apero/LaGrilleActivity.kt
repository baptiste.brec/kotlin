package com.example.apero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.example.apero.databinding.LagrilleLayoutBinding
import kotlin.random.Random


class LaGrilleActivity : AppCompatActivity() {

    private lateinit var binding: LagrilleLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = LagrilleLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.GrilleRetourbutton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        binding.GrilleGreenDetextView.setTextColor((4278255385/2)*2)
        binding.GrilleReddetextView.setTextColor((4294901760/2)*2)

        binding.GrilleLancerbutton.setOnClickListener {
            binding.GrilleGreenDetextView.text=String.format("%d",Random.nextInt(1,7))
            binding.GrilleReddetextView.text=String.format("%d",Random.nextInt(1,7))
        }

    }
}